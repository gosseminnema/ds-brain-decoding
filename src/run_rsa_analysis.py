from data_preparation import brain_dataset
import numpy as np
import scipy.stats


def main():
    dataset = brain_dataset.Pereira18Dataset("averaged")
    participants = ["M01", "M02", "M03", "M04", "M05", "M06", "M07", "M08", "M09", "M10", "M13", "M14", "M15", "M16",
                    "M17", "P01"]

    glove = dataset.dsm_data.squeeze()
    print(glove.shape)
    for p in participants:
        dataset.load_participant_data(p)
        brain = dataset.brain_data[0]
        rsa, p_val = analyze_rsa(brain, glove)
        print(f"{p}\t{rsa}\t{p_val}")


def analyze_rsa(source, target):
    # Compute pairwise correlations
    corr_pred = np.corrcoef(source)
    corr_targ = np.corrcoef(target)

    # Flatten upper triangle of both correlation matrices
    upp_tri_pred = get_upper_triangle(corr_pred)
    upp_tri_targ = get_upper_triangle(corr_targ)

    rsa_value, p_val = scipy.stats.pearsonr(upp_tri_pred, upp_tri_targ)
    return rsa_value, p_val


def get_upper_triangle(matrix):
    triangle = []

    n, m = matrix.shape
    for i in range(n):
        for j in range(m):
            triangle.append(matrix[i, j])

    return np.array(triangle)


if __name__ == '__main__':
    main()
