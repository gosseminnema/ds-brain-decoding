import argparse
import random

import numpy as np
import torch

from experiments import experiment
from models import factories as m_factories
from voxel_reduction import factories as rx_factories
from data_preparation import brain_dataset

random.seed(777777)
np.random.seed(22)
torch.manual_seed(10)


def main(args):

    # Get run name
    run_name = args.run_name

    # Initialize the dataset
    if args.dataset == "pereira18":
        load_extra_data = int(args.load_extra_data)
        dataset = brain_dataset.Pereira18Dataset(args.pereira18_paradigm, load_extra_data=load_extra_data)
    elif args.dataset == "mitchell08":
        dataset = brain_dataset.Mitchell08Dataset()
    else:
        raise ValueError("`dataset` should be one of ['pereira18', 'mitchell08']")

    # Augment data with neighbours?
    n_neighbours = int(args.add_nearest_neighbours.lower())
    if n_neighbours > 0:
        dataset.add_nearest_neighbours(n=n_neighbours)

    print(dataset.dsm_data.shape)
        
    # Parse the participant list
    participants = args.participants.split()

    # Initialize model factory
    n_epochs = int(args.epochs)
    
    if args.model_type == "pereira18":
        m_factory = m_factories.Pereira18Factory()

    elif args.model_type == "mlp":
        m_factory = m_factories.PerceptronFactory(use_dropout=False, non_linear=None, hidden=None, epochs=n_epochs)
    elif args.model_type == "mlp2": 
        m_factory = m_factories.PerceptronFactory(use_dropout=False, non_linear="relu", hidden=[(5000, 5000), (5000, 300)], epochs=n_epochs) # m08-mlp-2hidden-relu-e10

    elif args.model_type == "cnn":
        m_factory = m_factories.CNNModelFactory(pretrained=False, n_epochs=n_epochs)
    elif args.model_type == "cnn-alex":
        m_factory = m_factories.CNNModelFactory(pretrained=True, n_epochs=n_epochs)

    elif args.model_type == "plsr":
        m_factory = m_factories.PLSRModelFactory()
    elif args.model_type == "svr":
        m_factory = m_factories.SVRModelFactory()
    else:
        raise ValueError("`model_type` should be one of ['pereira18', 'mlp', 'cnn', 'plsr', 'svr']")

    # Initialize VoxelSelector factory (only one possibility at this point)
    sx_factory = rx_factories.Pereira18SelectorFactory()

    # Initialize VoxelScorer factory
    if args.scoring_algorithm == "pereira18":
        sc_factory = rx_factories.Pereira18ScorerFactory()
    elif args.scoring_algorithm == "simplified":
        sc_factory = rx_factories.SimplifiedScorerFactory()
    elif args.scoring_algorithm == "variability":
        sc_factory = rx_factories.VariabilityScorerFactory(int(args.var_threshold))
    elif args.scoring_algorithm == "None":
        sc_factory = None
    else:
        raise ValueError("`scoring_algorithm` should be one of ['pereira18', simplified', 'variability', 'None']")

    # Randomize or not?
    if args.randomize.lower() == "true":
        randomize = True
    elif args.randomize.lower() == "false":
        randomize = False
    else:
        raise ValueError("Value of `randomize` should be either 'false' or 'true'")

    # Center or not?
    if args.center.lower() == "true":
        center = True
    elif args.center.lower() == "false":
        center = False
    elif args.center.lower() == "only_brain":
        center = "only_brain"
    else:
        raise ValueError("Value of `center` should be either 'only_brain', 'false' or 'true'")

    # Get previous run name to load data from
    if args.prev_run:
        prev_run = args.prev_run
    else:
        prev_run = None

    # Load best voxels from a previous run?
    if args.load_best_voxels.lower() == "true":
        load_best_voxels = True
    elif args.load_best_voxels.lower() == "false":
        load_best_voxels = False
    else:
        raise ValueError("Value of `load_best_voxels` should be either 'false' or 'true'")

    # Get number of folds
    n_folds = int(args.n_folds)
    if not n_folds > 0:
        raise ValueError("`n_folds` must be a positive integer")

    # Run the experiment
    runner = experiment.ExperimentRunner(dataset, run_name, randomize, center, prev_run,
                                         load_best_voxels, n_folds)
    runner.run(participants, m_factory, sc_factory, sx_factory)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("run_name",
                        help="Name of the current run, will be used for naming output files (N.B.: using a run name "
                             "that already exists will cause the corresponding files to be overwritten")

    parser.add_argument("dataset",
                        help="Dataset to use, should be one of ['pereira18', 'mitchell08']")

    parser.add_argument("participants",
                        help="List of participants, should be a space-separated string. To combine participants, use 'mixed-P1-...-Pn"
                             "participants")

    parser.add_argument("--model_type",
                        help="Decoding model type, should be one of ['pereira18', 'mlp', 'mlp2', 'cnn']",
                        default="pereira18")

    parser.add_argument("--scoring_algorithm",
                        help="Voxel scoring algorithm to use, should be one of ['pereira18', 'simplified', "
                             "'variability', 'None']",
                        default="pereira18")
    
    parser.add_argument("--var_threshold",
                        help="variability threshold: when using variability scoring, ignore extremely variable voxels",
                        default=0)

    parser.add_argument("--pereira18_paradigm",
                        help="If the pereira18 dataset is selected, which stimuli should be used? One of ['averaged', "
                             "'all', 'pictures', 'sentences', 'wordclouds']",
                        default="averaged")

    parser.add_argument("--load_extra_data",
                        help="Load extra data? (Only implemented for pereira18, will add data from experiment 2 if "
                             "set to '2' or from experiment 3 if set to '3')",
                        default="0")

    parser.add_argument("--n_folds",
                        help="Number of cross-validation folds, set to 1 to disable cross-validation",
                        default=18)

    parser.add_argument("--randomize",
                        help="Should the dataset be randomized? One of ['true', 'false']",
                        default="true")

    parser.add_argument("--center",
                        help="Should the dataset be centered? One of ['true', 'false', 'only_brain']",
                        default="true")

    parser.add_argument("--prev_run",
                        help="If you want to use saved data from a previous run, specify the name of that run",
                        default="")

    parser.add_argument("--load_best_voxels",
                        help="Should best voxels be loaded from a previous run? One of ['true', 'false']. Requires "
                             "`prev_run` to be set",
                        default="false")

    parser.add_argument("--add_nearest_neighbours",
                        help="Augment the vocabulary with the 'n' nearest neighbours of each word (copy brain images)",
                        default="0")
    
    parser.add_argument("--epochs",
                        help="For MLP models, how many full runs over the training set should be made?",
                        default="10")


    main(parser.parse_args())
