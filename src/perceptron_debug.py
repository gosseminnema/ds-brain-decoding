from math import sqrt   
import random

import numpy as np
import torch

from sklearn.metrics import r2_score
from models import perceptron

torch.manual_seed(100)
np.random.seed(100)
random.seed(100)

CENTER_BRAIN = False
CENTER_GLOVE = False

def cosine_similarity(v1, v2):
    num = np.dot(v1, v2)
    den_a = np.dot(v1, v1)
    den_b = np.dot(v2, v2)
    return num / (sqrt(den_a) * sqrt(den_b))


def two_by_two(p1, p2, g1, g2):
    p1g1 = cosine_similarity(p1, g1)
    p1g2 = cosine_similarity(p1, g2)
    p2g1 = cosine_similarity(p2, g1)
    p2g2 = cosine_similarity(p2, g2)
    print("p1g1:", p1g1, "p1g2", p1g2, "p2g2:", p2g2, "p2g1:", p2g1)
    scores = [p1g1, p2g2, p2g1, p1g2]
    # if p1g1 > p1g2 and p2g2 > p2g1:
    # if np.argmax(scores) in [0,1]:
    if p1g1 + p2g2 > p1g2 + p2g1:
        return 1
    else:
        return 0


brain = np.load("../aurelie_working_example.npy")
glove = np.loadtxt("../data/Pereira-etal_2018/vectors_180concepts.GV42B300.txt")

ids = np.arange(180)
np.random.shuffle(ids)
train = ids[:170]
test = ids[170:]

if CENTER_BRAIN:
    mean_image = np.mean(brain[train], axis=0)
else:
    mean_image = np.zeros(5000)

if CENTER_GLOVE:
    mean_vector = np.mean(glove[train], axis=0)
else:
    mean_vector = np.zeros(300)

model = perceptron.PerceptronModel()
model.train(brain[train] - mean_image, glove[train] - mean_vector)

predictions = model.test(brain[test])
golds = []

i_pred = 0
for i_test in test:
    prediction = predictions[i_pred]
    gold = glove[i_test]
    golds.append(gold)
    
    print(i_test, cosine_similarity(prediction, gold), r2_score(prediction, gold))
    
    i_pred += 1

print("Running 2 by 2...")

score = 0
n = 0
for i in range(len(golds) - 1):
    for j in range(i + 1, len(golds)):
        score += two_by_two(predictions[i], predictions[j], golds[i], golds[j])
        n += 1
print("2 By 2:", score, n, score / n)