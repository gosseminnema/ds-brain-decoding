import random

import numpy as np
import sklearn.metrics as metrics

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import torchvision
from torch.utils import model_zoo
from torchvision.models.alexnet import model_urls

from models import model_base

from tqdm import tqdm, trange


class CNNModel(model_base.Model):

    def __init__(self, meta, mask, pretrained=False, conv_kernel_size=3, max_kernel_size=10, n_epochs=10):
        """
        :param meta: Matlab object (read with scipy.io) representing metadata for the brain images of the current
        participant. Needed to reconstruct 3d images
        :param mask: voxel mask
        """

        # Needed to reconstruct 3D images
        self.mask = mask
        self.dimensions = list(meta["dimensions"][0][0][0])
        self.indices3d = meta["indicesIn3D"][0][0][:, 0]
        self.n_epochs = n_epochs

        self.pretrained = pretrained
        if pretrained:
            alex = _ModifiedAlexNet()

            print(alex)
            self.model = alex

        else:
            self.model = _CNN(self.dimensions, conv_kernel_size, max_kernel_size)

    def train(self, train_examples, train_targets):

        num_epochs = self.n_epochs

        images = self.prepare_images(train_examples)

        self.model.train()

        if self.pretrained:

            criterion = nn.MSELoss()
            optimizer = optim.Adam(self.model.parameters(), lr=1e-3, weight_decay=1e-3)

        else:
            criterion = nn.CosineEmbeddingLoss()
            optimizer = optim.Adam(self.model.parameters(), lr=1e-3, weight_decay=1e-3)
            #optimizer = optim.Adam(self.model.parameters(), lr=0.1, weight_decay=1e-3) # faster LR when there are more examples?

        n_examples = len(images)
        train_idx = list(range(n_examples))

        for _ in trange(num_epochs, desc="Epochs: "):

            # Shuffle the training set to introduce some variation between epochs
            random.shuffle(train_idx)

            for i in train_idx:

                # Convert input arrays to tensors
                example = Variable(images[i], requires_grad=True)
                target = Variable(torch.FloatTensor([train_targets[i]]), requires_grad=False)

                # Clear gradients
                self.model.zero_grad()

                # Forward pass
                output = self.model(example)

                # Calculate loss and back-propagate
                if self.pretrained:
                    loss = criterion(output, target)
                else:
                    loss = criterion(output, target, torch.Tensor([1]))
                loss.backward()
                optimizer.step()

                if i % 20 == 0:
                    tqdm.write(str(loss.data[0]))
                    r2 = metrics.r2_score(output.data.numpy()[0], train_targets[i])
                    tqdm.write(str(r2))

    def prepare_images(self, images):

        # Pretrained AlexNet: convert to 2D, normalize and crop
        if self.pretrained:

            transforms = torchvision.transforms.Compose([
                torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                 std=[0.229, 0.224, 0.225]),
                # torchvision.transforms.ToPILImage(),
                # torchvision.transforms.Resize((224, 224)),
                # torchvision.transforms.ToTensor()
            ])

            images = [self._to_2d(im) for im in images]
            images = [torch.FloatTensor(im) for im in images]
            images = [transforms(im).unsqueeze(0) for im in images]

        # 'From scratch' model" convert examples to 3d
        else:
            images = [self._to_3d(im) for im in images]
            images = [torch.FloatTensor(im) for im in images]

        return images

    def test(self, test_examples):

        images = self.prepare_images(test_examples)

        # Put model in evaluation mode
        self.model.eval()

        # No gradient descent needed during testing (increases efficiency)
        with torch.no_grad():
            outputs = []
            for i in trange(len(test_examples)):
                example = images[i]
                output = self.model(example)
                outputs.append(output.data.numpy()[0])

        return np.array(outputs)

    @property
    def model_type(self):
        return "cnn"

    @property
    def params(self):
        return self.model.parameters()

    def _to_3d(self, image):
        # Initialize brain image with the right shape
        brain_3d = np.zeros(self.dimensions)

        # Get voxel mask
        masked_idx = np.where(self.mask)[0]

        # Flatten and insert voxels in the right places
        brain_3d = np.ravel(brain_3d, order="F")
        brain_3d[self.indices3d[masked_idx]] = image

        # Convert back to 3D
        brain_3d = np.reshape(brain_3d, newshape=self.dimensions, order="F")
        return brain_3d

    def _to_2d(self, image):

        image3d = self._to_3d(image)

        combined = np.zeros((image3d.shape[0] * 10, image3d.shape[1] * 10))

        xsize = image3d.shape[0]
        ysize = image3d.shape[1]

        z = 0

        for i in range(10):
            for j in range(10):

                if z >= 85:
                    break

                im = image3d[:, :, z]

                xcorner = image3d.shape[0] * i
                ycorner = image3d.shape[1] * j

                combined[xcorner:xcorner + xsize, ycorner:ycorner + ysize] = im

                z += 1

        # convert voxel activations to (grayscale) value in [0, 1]
        combined_interp = np.interp(combined, (combined.min(), combined.max()), (0, 1))

        # stack the grayscale values to get an RGB image
        combined_rgb = np.stack((combined_interp,) * 3, axis=0)

        return combined_rgb


class _CNN(nn.Module):

    def __init__(self, brain_dims, conv_kernel_size, max_kernel_size):
        super().__init__()

        # cnn05
        self.conv1 = nn.Conv3d(in_channels=1, out_channels=1, kernel_size=conv_kernel_size)
        self.max1 = nn.MaxPool3d(kernel_size=max_kernel_size)

        # Determine output size of the convolutional part
        self.in_shape = [1, 1] + brain_dims
        out = self.max1(self.conv1(torch.rand(self.in_shape)))
        out_shape = out.view(-1).shape

        self.fc = nn.Linear(out_shape[0], 300)

    def forward(self, x):

        # Apply convolution, ReLU, max pooling (x2) ==> shape [1, channels, ?, ?, ?]
        x = x.view(self.in_shape)
        x = self.max1(F.relu(self.conv1(x)))

        # Flatten the output ==> shape [channels * ? * ? * ?]
        x = x.view(1, -1)

        # Apply linear layer
        x = self.fc(x)

        return x


class _ModifiedAlexNet(torchvision.models.AlexNet):

    def __init__(self):
        super().__init__()

        # first copy all parameters (before changing any layers!)
        self.load_state_dict(model_zoo.load_url(model_urls['alexnet']))

        # replace layer 1 to work with higher-res images
        self.classifier[1] = nn.Linear(259584, 4096)

        # replace layer 6 to get the right output size
        in_features = self.classifier[6].in_features
        self.classifier[6] = nn.Linear(in_features, 300)

    def forward(self, x):

        x = self.features(x)
        x = x.view(x.size(0), 259584)
        x = self.classifier(x)

        return x
