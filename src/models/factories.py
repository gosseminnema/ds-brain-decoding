import abc

from models import pereira18, perceptron, convolution, model_base, plsr, svr


class ModelFactory(abc.ABC):
    """
    Object for creating `Model` instances. Using the Factory design pattern allows the ExperimentRunner.run() method
    to be generic (i.e. agnostic as to which subclass of `Model` should be used).
    This abstract base class specifies the interface that deriving classes should implement.
    """

    @abc.abstractmethod
    def create_model(self, n_voxels=5000, meta=None, mask=None):
        """
        :rtype: model_base.Model
        """
        pass


class Pereira18Factory(ModelFactory):
    """
    Generates new instances of the `Pereira18Model` class
    """

    def create_model(self, n_voxels=5000, meta=None, mask=None):
        return pereira18.Pereira18Model()


class PerceptronFactory(ModelFactory):
    """
    Generates new instances of the `PerceptronModel` class
    """

    def __init__(self, use_dropout=False, non_linear=None, hidden=None, epochs=5):
        self.use_dropout = use_dropout
        self.non_linear = non_linear
        self.hidden = hidden
        self.epochs = epochs

    def create_model(self, n_voxels=5000, meta=None, mask=None):
        return perceptron.PerceptronModel(n_voxels, use_dropout=self.use_dropout, non_linear=self.non_linear,
                                          hidden=self.hidden, epochs=self.epochs)


class CNNModelFactory(ModelFactory):
    """
    Generates new instances of the `CNNModel` class
    """

    def __init__(self, pretrained=False, conv_kernel_size=3, max_kernel_size=10, n_epochs=10):
        self.conv_kernel_size = conv_kernel_size
        self.max_kernel_size = max_kernel_size
        self.pretrained = pretrained
        self.n_epochs = n_epochs

    def create_model(self, n_voxels=5000, meta=None, mask=None):
        return convolution.CNNModel(meta, mask, pretrained=self.pretrained, conv_kernel_size=self.conv_kernel_size,
                                    max_kernel_size=self.max_kernel_size, n_epochs=self.n_epochs)


class PLSRModelFactory(ModelFactory):

    def create_model(self, n_voxels=5000, meta=None, mask=None):
        return plsr.PLSRModel()


class SVRModelFactory(ModelFactory):

    def create_model(self, n_voxels=5000, meta=None, mask=None):
        return svr.SVRModel()
