import sklearn.svm
import sklearn.multioutput

from models import model_base


class SVRModel(model_base.Model):
    def __init__(self):
        self.model = sklearn.multioutput.MultiOutputRegressor(estimator=sklearn.svm.SVR())

    def train(self, train_examples, train_targets):
        self.model.fit(train_examples, train_targets)

    def test(self, test_examples):
        return self.model.predict(test_examples)

    @property
    def model_type(self):
        return "svr"

    @property
    def params(self):
        return None
