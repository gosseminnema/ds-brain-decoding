import random

from sklearn import metrics as metrics

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from tqdm import trange, tqdm

from models.model_base import Model

import numpy as np


class PerceptronModel(Model):

    def __init__(self, n_voxels, use_dropout=False, non_linear=None, hidden="default", epochs=5):
        """
        :param use_dropout: apply a dropout layer between the two linear layers?
        :param non_linear: one of ["relu", "tanh"], will be applied after each linear layer
        :param hidden: list of hidden layer size tuples (in, out) [includes the output layer]
        """

        self.model = _MLP(n_voxels, use_dropout=use_dropout, non_linear=non_linear, hidden=hidden)
        self.epochs = epochs

    @property
    def model_type(self):
        return "mlp"

    @property
    def params(self):
        return list(self.model.parameters())

    def train(self, train_examples, train_targets, optim_method="adam", objective="cosine"):

        # Put model in training mode
        self.model.train()
        
        # Initialize optimzer and loss function
        if optim_method == "adam":
            optimizer = optim.Adam(self.model.parameters(), lr=1e-3, weight_decay=1e-3)
        else:
             optimizer = optim.SGD(self.model.parameters(), lr=1e-3)
        if objective == "cosine":
            criterion = nn.CosineEmbeddingLoss()
        else:
            criterion = nn.MSELoss()

        n_examples = len(train_examples)
        train_idx = list(range(n_examples))

        for _ in trange(self.epochs, desc="Epochs: "):
            
            # Shuffle the training set to introduce some variation between epochs
            random.shuffle(train_idx)

            for i in train_idx:
                                
                # Convert input arrays to tensors
                example = Variable(torch.FloatTensor([train_examples[i]]), requires_grad=True)
                target = Variable(torch.FloatTensor([train_targets[i]]), requires_grad=False)

                # Clear gradients
                self.model.zero_grad()

                # Forward pass
                output = self.model(example)

                # Calculate loss and back-propagate
                loss = criterion(output, target, torch.Tensor([1]))
                #loss = criterion(output, target)
                loss.backward()
                optimizer.step()
                
                if i % 20 == 0:
                    tqdm.write(str(loss.data[0]))
                    r2 = metrics.r2_score(output.data.numpy()[0], train_targets[i])
                    tqdm.write(str(r2))

    def test(self, test_examples):
        
        # Put model in evaluation mode
        self.model.eval()

        # No gradient descent needed during testing (increases efficiency)
        with torch.no_grad():

            outputs = []
            for i in trange(len(test_examples)):
                example = Variable(torch.FloatTensor([test_examples[i]]))
                output = self.model(example)
                outputs.append(output.data.numpy()[0])

        return np.array(outputs)


class _MLP(nn.Module):
    """
    PyTorch module representing the model itself
    """

    def __init__(self, n_input_units, use_dropout=False, non_linear=None, hidden="default"):
        """
        :param use_dropout: apply a dropout layer between the two linear layers?
        :param non_linear: one of ["relu", "tanh"], will be applied after each linear layer
        :param hidden: list of hidden layer size tuples (in, out) [includes the output layer]
        """
        
        super().__init__()
        
        if hidden == "default":
            self.fc = [
                nn.Linear(n_input_units, 300),
                nn.Linear(300, 300)
            ]
            
        elif hidden:
            self.fc = [nn.Linear(n_input_units, hidden[0][0])] + [nn.Linear(h[0], h[1]) for h in hidden]
        
        else:
            self.fc = [nn.Linear(n_input_units, 300)]
            
        for i, layer in enumerate(self.fc):
            for j, param in enumerate(layer.parameters()):
                self.register_parameter(f"fc{i}_{j}", param)
        
        self.drop = nn.Dropout(p=0.5)
        
        self.use_dropout = use_dropout
        self.non_linear = non_linear
        
        # Apply He et al. (2015) initialization when using RELU
        if non_linear == "relu":
            for layer in self.fc:
                nn.init.kaiming_uniform_(layer.weight, nonlinearity="relu")

    def _apply_non_linear(self, x):
        if self.non_linear == "relu":
            x = F.relu(x)

        if self.non_linear == "tanh":
            x = F.tanh(x)

        return x

    def _apply_dropout(self, x):
        if self.use_dropout:
            x = self.drop(x)

        return x

    def forward(self, x):
        
        # For (n-1) layers: linear + activation + dropout (if turned on)
        for layer in self.fc[:-1]:
            x = layer(x)
            x = self._apply_non_linear(x)
            x = self._apply_dropout(x)
        
        # Last layer: only linear layer
        x = self.fc[-1](x)
        
        return x
