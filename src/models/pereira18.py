import sys

from models.model_base import Model

import numpy as np


class Pereira18Model(Model):
    """
    Python translation of Pereira et al. (2018)'s ridge regression model. Learns a separate regression model for each
    target dimension and automatically chooses a regularization parameter for each target dimension.
    """

    def __init__(self):

        # Weight matrix, shape (num_target_dimensions, num_brain_dimensions + 1), includes intercept
        self.weights = None

        # 1D array of the regularization parameter used for each target dimension
        self.regularization_params = None

    @property
    def model_type(self):
        return "pereira18"

    @property
    def params(self):
        return {"weights": self.weights, "regularization_params": self.regularization_params}

    # Based on the `learnDecoder` MATLAB script published by Pereira et al.
    def train(self, train_examples, train_targets):

            # Append a vector of ones so we can learn weights and biases together
            extra_ones = np.ones((train_examples.shape[0], 1))
            train_examples = np.hstack([train_examples, extra_ones])

            # Do singular value decomposition (SVD) on the training set
            svd_examples = self._decompose_training_data(train_examples)
                
            # Define range of possible regularization params
            params = [1, 0.5, 5, 0.1, 10, 0.01, 100, 0.001, 1000, 0.0001, 10000, 0.00001, 100000, 0.000001, 1000000]

            # Find the parameter that works best for each dimension
            min_err_idx = self._choose_reg_params(train_examples, train_targets, svd_examples, params)

            self.weights, self.regularization_params = \
                self._calculate_weights(train_examples, train_targets, svd_examples, min_err_idx, params)

    @staticmethod
    def _calculate_weights(train_examples, train_targets, svd_examples, min_err_idx, params):
        """
        Train a separate regression for each dimension of the target space
        :param train_examples: array of shape (num_examples, num_voxels)
        :param train_targets: array of shape (num_examples, num_dimensions)
        :param svd_examples: decomposed version of `train_examples`
        :param params: range of possible regularization parameters
        :param min_err_idx: indices of the best best regularization parameters
        :return:
        """

        num_target_dimensions = train_targets.shape[1]
        num_example_dimensions = train_examples.shape[1]

        # 1D array of regularization params that were used for each dimension
        r = np.zeros(num_target_dimensions)

        # Initialize weight matrix
        weight_matrix = np.zeros((num_example_dimensions, num_target_dimensions))

        # Find the weights for each target
        for cur_target in range(num_target_dimensions):
            regularization_param = params[min_err_idx[cur_target]]
            r[cur_target] = regularization_param

            # got good param, now obtain weights
            dlambda = svd_examples["d"] + np.dot(regularization_param, np.identity(len(svd_examples["d"])))
            dlambda_inv = np.diag(1 / np.diag(dlambda))
            klambda_inv = np.dot(np.dot(svd_examples["v"], dlambda_inv), svd_examples["u"].T)

            # Solve for weight matrix so we can compute residual
            weight_matrix[:, cur_target] = np.dot(np.dot(train_examples.T, klambda_inv),
                                                  train_targets[:, cur_target])
        return weight_matrix, r

    @staticmethod
    def _choose_reg_params(train_examples, train_targets, svd_examples, params):
        """
        Find the best regularization parameter for each dimension
        :param train_examples: array of shape (num_examples, num_voxels)
        :param train_targets: array of shape (num_examples, num_dimensions)
        :param svd_examples: decomposed version of `train_examples`
        :param params: range of possible training parameters
        :return: array of shape (num_target_dimensions)
        """

        num_target_dimensions = train_targets.shape[1]
        # cross-validation error
        cv_err = np.zeros((len(params), num_target_dimensions))
        for i in range(len(params)):
            regularization_param = params[i]

            # Now we can obtain Kinv for any lambda doing Kinv = V * (D + lambda*I)^-1 U'
            dlambda = svd_examples["d"] + np.dot(regularization_param, np.identity(len(svd_examples["d"])))
            dlambda_inv = np.diag(1 / np.diag(dlambda))
            klambda_inv = np.dot(np.dot(svd_examples["v"], dlambda_inv), svd_examples["u"].T)

            # Compute pseudoinverse of linear kernel
            kp = np.dot(train_examples.T, klambda_inv)

            # Compute S matrix of Hastie Trick X*KP
            s = np.dot(train_examples, kp)

            # Solve for weight matrix so we can compute residual
            weight_matrix = np.dot(kp, train_targets)

            s_norm = np.tile(1 - np.diag(s).reshape(len(train_examples), 1), (1, num_target_dimensions))

            y_diff_mat = train_targets - (np.dot(train_examples, weight_matrix))
            y_diff_mat = y_diff_mat / s_norm
            cv_err[i] = (1 / len(train_examples)) * sum(y_diff_mat * y_diff_mat)

        min_err_idx = np.argmin(cv_err, axis=0)
        return min_err_idx

    @staticmethod
    def _decompose_training_data(train_examples):
        """
        Helper method, performs SVD on the training data
        :param train_examples: array of shape (num_examples, num_voxels)
        :return: dictionary, with entries "u", "d", "v"
        """

        k0 = np.dot(train_examples, train_examples.T)
        u, d, v = np.linalg.svd(k0, full_matrices=True)

        # Reformat the output so that it has the same form as in MATLAB
        d = np.diag(d)
        v = v.T

        return {"u": u, "v": v, "d": d}

    def test(self, test_examples):

        weights = self.weights[:-1]
        intercept = self.weights[-1]
        
        return np.dot(test_examples, weights) + intercept
