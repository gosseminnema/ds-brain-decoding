import sklearn.cross_decomposition

from models import model_base


class PLSRModel(model_base.Model):

    def __init__(self):
        self.model = sklearn.cross_decomposition.PLSRegression()

    def train(self, train_examples, train_targets):
        self.model.fit(train_examples, train_targets)

    def test(self, test_examples):
        return self.model.predict(test_examples)

    @property
    def model_type(self):
        return "PLSR"

    @property
    def params(self):
        return None
