"""
This script finds nearest neighbours for each of the words in the Pereira et al. '18 or Mitchell et al. '08 datasets.
For Pereira et al. '18 the file '../data/Pereira-etal_2018/glove.42B.300d.w2v.txt' is required. This file can be
acquired by downloading http://nlp.stanford.edu/data/glove.42B.300d.zip and converting this file to W2V format using
gensim (https://radimrehurek.com/gensim/scripts/glove2word2vec.html).

The GitLab repo contains the output vector files but not the original GloVe vector files due to size restrictions.
"""


import numpy as np
import gensim

import json

from tqdm import tqdm


def main(dataset):

    if dataset == "pereira18":
        nn_path = "../data/Pereira-etal_2018/nearest_neighbours.json"
        vocab_path = "../data/Pereira-etal_2018/stimuli_180+neighbours.txt"
        w2i_path = "../data/Pereira-etal_2018/stimuli_180+neighbours_w2i.json"
        vectors_path = "../data/Pereira-etal_2018/vectors_180+neighbours.npy"

    elif dataset == "mitchell08":
        nn_path = "../data/Mitchell-etal_2008/nearest_neighbours.json"
        vocab_path = "../data/Mitchell-etal_2008/dictionary+neighbours.txt"
        w2i_path = "../data/Mitchell-etal_2008/dictionary+neighbours_w2i.json"
        vectors_path = "../data/Mitchell-etal_2008/vectors+neighbours.npy"

    else:
        raise ValueError

    print("Loading nearest neighbour list ...")
    with open(nn_path, encoding="utf-8") as f:
        neighbours = json.load(f)

    print("Loading GloVe model in gensim ...")
    model = gensim.models.KeyedVectors.load_word2vec_format("../data/Pereira-etal_2018/glove.42B.300d.w2v.txt")

    vocabulary = []
    vectors = []
    w2index = {}

    for word in tqdm(neighbours):
        tqdm.write(word)
        vocabulary.append(word)
        vectors.append(model[word])
        w2index[word] = len(vocabulary) - 1

        for neighbour, distance in neighbours[word]:
            tqdm.write(neighbour)
            vocabulary.append(neighbour)
            vectors.append(model[neighbour])
            w2index[neighbour] = len(vocabulary) - 1

        tqdm.write("\n")

    vectors = np.array(vectors)

    with open(vocab_path, "w", encoding="utf-8") as f:
        for w in vocabulary:
            f.write(f"{w}\n")

    with open(w2i_path, "w", encoding="utf-8") as f:
        json.dump(w2index, f)

    np.save(vectors_path, vectors)


if __name__ == '__main__':
    main(input("Select a dataset ['pereira18', 'mitchell08'] >>> "))
