import collections
from run_analysis import load_data
import numpy as np

# Define arguments for load_data
Args = collections.namedtuple(
    typename="Args",
    field_names=[
        "prev_run",
        "randomize",
        "dataset",
        "pereira18_paradigm"
    ])


def main():

    # Get predictions
    args = Args("p18a-simpl-p18", "true", "pereira18", "averaged")
    dataset, _, predictions, targets = load_data(args)

    for participant in predictions:
        np.savetxt(f"../vectors_for_david/{participant}.txt", predictions[participant])

    np.savetxt("../vectors_for_david/targets.txt", targets)


if __name__ == '__main__':
    main()
