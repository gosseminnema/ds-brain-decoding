import sys
import os
import numpy as np
import scipy.io


def main():

    path = sys.argv[1]
    examples = scipy.io.loadmat(path)["examples"]

    name, _ = os.path.splitext(path)
    np.save(name + ".npy", examples)


if __name__ == "__main__":
    main()
