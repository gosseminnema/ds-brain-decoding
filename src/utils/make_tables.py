"""
Helper script for pre-formatting tables (can be printed in tablesgenerator.com)
"""

import collections
from IPython.utils import io

from run_analysis import run_analysis

Args = collections.namedtuple(
    typename="Args",
    field_names=[
        "prev_run",
        "randomize",
        "dataset",
        "pereira18_paradigm"
    ])


experiments = [
    "p18-random",  # Random
    "p18a-novoxsel-p18",  # Ridge
    "p18-extra-p18",  # Ridge+exp2
    "p18multi-ridge-novoxsel-center",  # Ridge+para
    "p18-aug6-p18",  # Ridge+aug
    "_p18_shuff",  # Perceptron (w/ vx selection)
    "_best_cnn_all",  # CNN
]

if __name__ == "__main__":

    for exp in experiments:

        args = Args(prev_run=exp, randomize="true", dataset="pereira18", pereira18_paradigm="averaged")

        # run the analyzers
        with io.capture_output() as _:
            p_analyzer, c_analyzer, n_analyzer, r_analyzer, r2_analyzer, pat1_analyzer, rsa_analyzer = \
                run_analysis(args)

        # columns: Pair, Rank, Cos, R2, NN, RSA

        scores = []
        for analyzer in (p_analyzer, r_analyzer, c_analyzer, r2_analyzer, n_analyzer, rsa_analyzer):

            df = analyzer.create_scores_df()

            score_column = "score_avg" if "score_avg" in df.columns else "score"

            # subcolumns: I_B, I_A, A
            indiv_best = df[df["participant"] != "averaged"][score_column].max()
            indiv_avg = df[df["participant"] != "averaged"][score_column].mean()
            avg = df[df["participant"] == "averaged"][score_column].item()
            scores.extend([indiv_best, indiv_avg, avg])

        missing_data = exp in []
        unbalanced_avg = exp == "p18-extra-p18"
        print("\t".join(f"{s:.2f}{'**' if missing_data else '*' if unbalanced_avg else ''}" for s in scores))




