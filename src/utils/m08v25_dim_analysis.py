import collections
from run_analysis import load_data
import numpy as np
import sklearn.metrics

# Define arguments for load_data
Args = collections.namedtuple(
    typename="Args",
    field_names=[
        "prev_run",
        "randomize",
        "dataset",
        "pereira18_paradigm"
    ])

dimensions = ['approach', 'break', 'clean', 'drive', 'eat', 'enter', 'fear', 'fill', 'hear', 'lift', 'listen',
              'manipulate', 'move', 'neared', 'open', 'push', 'ride', 'rub', 'run', 'say', 'see', 'smell', 'taste',
              'touch', 'wear']


def main():

    # Get predictions
    args = Args("m08v25-simpl-p18", "true", "mitchell08v25", "averaged")
    dataset, _, predictions, targets = load_data(args)

    for participant in predictions:

        print(f"Participant: {participant}")
        r2 = sklearn.metrics.r2_score(targets, predictions[participant], multioutput="raw_values")
        for i, dim in enumerate(dimensions):
            print(f"{i}\t{dim}\t{r2[i]}")
        print()


if __name__ == '__main__':
    main()
