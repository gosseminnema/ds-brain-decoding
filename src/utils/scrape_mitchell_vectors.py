import bs4
import requests
import re
import numpy as np


def main():

    with open("../data/Mitchell-etal_2008/dictionary.txt", encoding="utf-8") as f:
        vocabulary = [w.strip() for w in f.readlines()]

    print(vocabulary)

    r = requests.get("http://www.cs.cmu.edu/~tom/science2008/semanticFeatureVectors.html")
    soup = bs4.BeautifulSoup(r.text, "lxml")

    # Vectors
    vectors = []

    # List of 'feature verbs'
    features = []

    for w in vocabulary:
        word_link = soup.find("a", attrs={"name": w})
        vector_list = word_link.findNext("ul")
        feature_to_value = {}

        # For 'cow', the value for 'say' is missing; set it to zero
        if w == "cow":
            feature_to_value["say"] = 0.0

        for feature_info in vector_list.findAll("li"):
            m = re.match(r"(\w+)\s+(\w*)\s*(\w+)\s+\((.+)\)", feature_info.text)
            word = m.groups()[0]
            value = float(m.groups()[-1])
            feature_to_value[word] = value

        # Get key-value pairs and sort them so that we have the same order every time
        word_value_pairs = sorted(feature_to_value.items())

        # Add vector to the list of vectors
        vector = [pair[1] for pair in word_value_pairs]
        vectors.append(vector)

        # Get the list of features (but only the first time)
        if not features:
            features = [pair[0] for pair in word_value_pairs]

    # Convert to numpy
    vectors = np.array(vectors)
    print(vectors)

    print(features)

    np.savetxt("../data/Mitchell-etal_2008/vectors/mitchell-25.txt", vectors)


if __name__ == '__main__':
    main()
