import gensim
import json

with open("../data/Mitchell-etal_2008/dictionary.txt", encoding="utf-8") as f:
    vocab = [w.strip() for w in f.readlines()]

model = gensim.models.KeyedVectors.load_word2vec_format("../data/Pereira-etal_2018/glove.42B.300d.w2v.txt",
                                                        limit=100000)

neighbours = {}
for w in vocab:
    neighbours[w] = model.most_similar(w, topn=100)

with open("../data/Mitchell-etal_2008/nearest_neighbours.json", "w", encoding="utf-8") as f:
    json.dump(neighbours, f)
