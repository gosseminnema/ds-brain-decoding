import sklearn.metrics as metrics
import numpy as np

from tqdm import tqdm

import sys

from data_preparation import data_splitting, brain_dataset
from experiments import save_outputs, load_outputs
from models import factories as m_factories
from voxel_reduction import factories as vx_factories

import pdb


class ExperimentRunner:
    """
    Handles the process of running the experiment
    """
    dataset: brain_dataset.BrainDataset

    def __init__(self, dataset, run_name, randomize=True, center=True, prev_run=None, load_best_voxels=False,
                 n_folds=18):
        """

        :param dataset:
        :param run_name:
        :param randomize: randomize the dataset
        :param center: center the data in each fold; one of [True, "only_brain", False] (if "only_brain" is selected,
        only the brain images, and not the meaning vectors, will be centered)
        :param prev_run: previous run name to load data from
        :param load_best_voxels: use previously calculated best_voxels
        :param n_folds: number of cross-validation folds
        """

        self.dataset = dataset
        self.run_name = run_name
        self.options = {
            "randomize": randomize,
            "center": center,
            "prev_run": prev_run,
            "load_best_voxels": load_best_voxels,
            "load_random_idx": prev_run is not None,
            "n_folds": n_folds,
            "save_voxel_scores": False,  # TODO add CLI option for this
            "save_model_params": False
        }

    def run(self, participants, m_factory: m_factories.ModelFactory, sc_factory: vx_factories.ScorerFactory,
            sx_factory: vx_factories.SelectorFactory):
        """
        Run the experiment
        :param participants: list of participant IDs
        :param m_factory: object for creating instances of the desired model type
        :param sc_factory: object for creating instances of the desired VoxelScorer type
        :param sx_factory: object for creating instances of the desired VoxelSelector type
        :return:
        """

        # Initialize output writer
        saver = save_outputs.OutputSaver(f"../output/{self.run_name}")

        # Initialize output loader
        if self.options["prev_run"] is not None:
            loader = load_outputs.OutputLoader(f"../output/{self.options['prev_run']}")
        else:
            loader = None

        # Check and randomize the dataset
        if not self.dataset:
            raise ValueError("Experiment cannot be run without a dataset")

        # Randomize the dataset
        if self.options["randomize"]:

            # Use exiting indices
            if self.options["load_random_idx"]:
                self.dataset.randomize(prev_idx=loader.load_random_idx())

            # Generate new indices
            else:
                self.dataset.randomize()
                
            saver.save_random_idx(self.dataset.random_indices)

        # Run the experiment for each of the participants
        for p in tqdm(participants, desc="Participants"):

            # Load brain imaging data
            if p.startswith("mixed"):
                #pdb.set_trace()
                self.dataset.load_mixed_participants(participants=p.split("-")[1:])
            else:
                self.dataset.load_participant_data(p)

            # Run cross_validation
            splitter = data_splitting.DataSplitter(self.dataset, self.options["n_folds"])
            for i, split_data in tqdm(enumerate(splitter), desc="Splits", total=self.options["n_folds"]):
                self._run_fold(p, i, split_data, m_factory, sc_factory, sx_factory, saver, loader)

    def _run_fold(self, p, i, sd: data_splitting.SplitData, m_factory: m_factories.ModelFactory,
                  sc_factory: vx_factories.ScorerFactory, sx_factory: vx_factories.SelectorFactory,
                  saver: save_outputs.OutputSaver, loader: load_outputs.OutputLoader):
        """
        Execute a cross-validation fold.
        :param p: participant id
        :param i: fold index
        :param m_factory: see documentation for `run()`
        :param sc_factory: see documentation for `run()`
        :param sx_factory: see documentation for `run()`
        :param sd: dataset associated with the current fold
        :param saver: OutputSaver object
        :return: (predictions, r2_score) tuple
        """

        # Center the data if necessary
        if self.options["center"] == "only_brain":
            sd.center(only_brain=True)
        elif self.options["center"]:
            sd.center()

        # Unpack train data (concatenate example sets if there are more than one)
        n_examples, n_sets, n_voxels = sd.data["brain"]["train"].shape
        _, n_vectors, n_dimensions = sd.data["dsm"]["train"].shape
        train_examples = sd.data["brain"]["train"].reshape(n_examples * n_sets, n_voxels)
        train_examples = train_examples.repeat(n_vectors, axis=0)
        train_targets = sd.data["dsm"]["train"].reshape(n_examples * n_vectors, n_dimensions)
        train_targets = train_targets.repeat(n_sets, axis=0)
        test_targets = np.array([vectors[0] for vectors in sd.data["dsm"]["test"]])

        # Add extra data (if appropriate)
        if sd.data["brain"]["extra"] is not None:
            
            # Reshape extra data so that we can concatenate it
            extra_examples = sd.data["brain"]["extra"].squeeze()
            extra_targets = sd.data["dsm"]["extra"].squeeze()
            
            # Do the concatenation
            train_examples = np.concatenate([train_examples, extra_examples])
            train_targets = np.concatenate([train_targets, extra_targets])

        # Skip voxel selection?
        if not sc_factory:
            if self.dataset.voxel_mask is not None:
                best_vx = np.where(self.dataset.voxel_mask)[0]
            else:
                best_vx = np.arange(n_voxels)
            
        # Load existing best voxels if they exist            
        elif self.options["load_best_voxels"]:
            best_vx = loader.load_best_vx_idx(p, i)

        # If not, calculate voxel scores
        else:
            # Run the voxel scoring algorithm
            scorer = sc_factory.create_scorer()
            selector = sx_factory.create_selector()

            scorer.calculate_scores(train_examples, train_targets,
                                    meta=self.dataset.meta,
                                    voxel_mask=self.dataset.voxel_mask)

            # Find the best voxels and save them
            best_vx = scorer.get_best_scores(5000, selector)

            # Save the scores and best voxels
            saver.save_best_vx_idx(p, i, best_vx)
            
            if self.options["save_voxel_scores"]:
                saver.save_voxel_scores(p, i, scorer.scores, self.dataset.voxel_mask)

        # Create the model
        model = m_factory.create_model(n_voxels=len(best_vx), meta=self.dataset.meta, mask=self.dataset.voxel_mask)
                
        # Train the model and save the parameters
        try:
            model.train(train_examples[:, best_vx], train_targets) 
            if self.options["save_model_params"]:
                saver.save_model_params(p, i, {"model": model.model_type, "params": model.params})

            # Generate predictions and save them
            predictions = model.test(sd.data["brain"]["test"][:, best_vx])
            saver.save_predictions(p, i, predictions)

            # Get R2 score
            try:
                r2_score = metrics.r2_score(test_targets, predictions)
                tqdm.write(f"R2 score for fold {i}: {r2_score}")
            except:
                r2_score = None
                tqdm.write(f"Error calculating R2 score for fold {i}")

            return predictions, r2_score
        
        except np.linalg.linalg.LinAlgError:
            print(f"Error when training participant {p} fold {i}", file=sys.stderr)
            np.save(f"error-{p}-{i}", train_examples[:, best_vx])
