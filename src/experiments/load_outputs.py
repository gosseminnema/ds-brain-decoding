import os
import re
import glob
import pickle
import tarfile

import numpy as np

from data_preparation import brain_dataset, data_splitting


class OutputLoader:
    """
    Load output files from earlier runs for use by an ExperimentRunner object
    """

    def __init__(self, path):
        """
        :param path: path to the folder containing the output files
        """

        self.path = path
        self.subdirs = ("voxel_scores", "best_vx_idx", "model_params", "predictions", "random_idx")

    def load_best_vx_idx(self, participant_id, cv_split):
        """
        Load the array of best voxels for a given cross-validation fold
        :param participant_id: participant ID
        :param cv_split: fold number
        :return: array of shape (num_examples_in_split)
        """

        file = os.path.join(self.path, self.subdirs[1], f"{participant_id}-{cv_split}.npy")
        return self._load(file)

    def load_random_idx(self):
        """
        Load the array of indices used for randomization
        :return: array of shape (num_examples)
        """

        file = os.path.join(self.path, self.subdirs[4], "random_idx.npy")
        return self._load(file)

    def load_predictions(self, participant_id, random_idx=None):
        """
        Load prediction data for a given participant
        :param participant_id: participant ID
        :param random_idx: array of indices used for randomization. If given, will be used to restore the original order
        of the predictions
        :return: array of shape (num_predictions, num_dimensions)
        """

        files = glob.glob(os.path.join(self.path, self.subdirs[3], f"{participant_id}-*.npy"))
        folds = [None] * len(files)

        for f in files:
            basename = os.path.basename(f)
            
            # Parse the fold ID from the filename
            m = re.match(f"{participant_id}-(\d*)\.npy", basename)
            fold = int(m.group(1))

            folds[fold] = self._load(f)

        n_examples = folds[0].shape[0] * len(folds)
        n_dimensions = folds[0].shape[1]

        # Flatten the prediction array
        # all_predictions = np.array([prediction for fold in folds for prediction in fold])
        all_predictions = np.empty((n_examples, n_dimensions))

        # Re-generate the testing indices for each fold
        labels_group = 1 + np.arange(1, n_examples + 1) % len(folds)
        groups = np.unique(labels_group)

        for i, group in enumerate(groups):
            mask = labels_group == group
            idx_test = np.where(mask)[0]
            if random_idx is not None:
                orig_idx_test = random_idx[idx_test]
            else:
                orig_idx_test = idx_test
            all_predictions[orig_idx_test] = folds[i]

        return all_predictions

    def load_participants(self):
        """
        Load the list of participants whose output data is present in the output directory
        :return: list of participant IDs
        """
        participants = set()

        for f in glob.glob(os.path.join(self.path, self.subdirs[3], "*.npy")):

            m = re.match("(.*)-\d*\.npy", os.path.basename(f))
            participants.add(m.group(1))

        return sorted(participants)

    @staticmethod
    def _load(file, pickled=False):
        if pickled:

            # Extract the tarball
            with tarfile.open(f"{file}.tar.gz", "r:gz") as tar_f:
                f = tar_f.extractfile(f"{file}.pickle")
                return pickle.load(f)

        else:
            return np.load(file)




