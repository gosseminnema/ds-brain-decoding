import abc
import math

import numpy as np
import scipy.stats
import sklearn.linear_model

from tqdm import tqdm

from voxel_reduction import voxel_selection


class VoxelScorer(abc.ABC):

    def __init__(self):
        self.scores = None

    @abc.abstractmethod
    def calculate_scores(self, examples, targets, meta=None, lmbda=1, voxel_mask=None):
        """
        Calculate a matrix of voxel scores.
        :param examples:
        :param targets:
        :param meta: `meta` object with information about neighbouring voxels. If supplied, a searchlight algorithm will
        be applied. Only works for Pereira et al.'s dataset
        :param lmbda: regularization parameter
        :param voxel_mask:
        :return:
        """
        pass

    def get_best_scores(self, n_best_voxels, selector: voxel_selection.VoxelSelector):
        """
        Given a score matrix, find the 'n' best voxels
        :param n_best_voxels: number of voxels to select
        :param selector: object implementing the selection algotihm
        :return:
        """
        return selector.select_best_voxels(self.scores)[-n_best_voxels:]


class Pereira18VoxelScorer(VoxelScorer):
    """
    Implements the voxel scoring algorithm from Pereira et al.'s paper.
    """

    # Based on Pereira et al.'s `learnVoxelwiseTargetPredictionModels.mat` script
    def calculate_scores(self, examples, targets, meta=None, lmbda=1, voxel_mask=None):

        n2, n_brain_dimensions = examples.shape
        n1, n_dsm_dimensions = targets.shape

        assert n1 == n2, "`targets` must have as many rows as `examples`"
        n = n1

        if voxel_mask is None:
            voxel_mask = np.ones(n_brain_dimensions)

        # 10-fold
        labels_group = 1 + np.arange(1, n + 1) % 10

        use_searchlight = meta is not None

        # Pre-compute a few things
        targets_z = scipy.stats.zscore(targets)
        predicted = np.zeros((n, n_dsm_dimensions))
        scores = np.zeros((n_dsm_dimensions, n_brain_dimensions))  # #targets x #voxel

        # Groups to use in cross-validation
        groups = np.unique(labels_group)
        n_groups = len(groups)

        indices_test = []
        indices_train = []
        targets_per_group = []
        for ig in range(n_groups):
            mask = labels_group == groups[ig]
            indices_test.append(np.where(mask)[0])
            indices_train.append(np.where(np.invert(mask))[0])
            targets_per_group.append(targets[indices_train[ig]])

        # voxelwise loop
        # (cross-validation is run for each voxel + searchlight (optionally))
        for v in tqdm(range(n_brain_dimensions), desc="Voxels"):

            if voxel_mask[v]:

                if use_searchlight:
                    # find the neighbours of each voxel and assemble a matrix with:
                    # - value of the voxels across all examples
                    # - values of the neighbours across all examples
                    # - column of ones (to use in regression)
                    nn = meta["numberOfNeighbours"][0][0][v][0]
                    nn1 = nn + 1
                    neighbours = np.append(np.array([v]), meta["voxelsToNeighbours"][0][0][v, :nn] - 1)
                    regularization_matrix = lmbda * np.identity(nn1)

                else:
                    # neighbourhood is the voxel by itself
                    neighbours = np.array([v])
                    regularization_matrix = lmbda

                data = examples[:, neighbours]

                # run cross-validation loop
                for ig in range(n_groups):
                    tmp = data[indices_train[ig]]

                    # more numerically stable version of the normal equations
                    betas = np.linalg.solve(np.dot(tmp.T, tmp) + regularization_matrix,
                                            np.dot(tmp.T, targets[indices_train[ig]]))

                    predicted[indices_test[ig]] = np.dot(data[indices_test[ig]], betas)

                scores[:, v] = np.sum(targets_z * scipy.stats.zscore(predicted), 0) / (n - 1)

        self.scores = scores


class SimplifiedVoxelScorer(VoxelScorer):
    """
    Simplified and faster version of the voxel scoring algorithm: skips cross-validation and uses an of-the-shelf
    ridge regression model from sklearn.
    """

    def calculate_scores(self, examples, targets, meta=None, lmbda=1, voxel_mask=None):

        if voxel_mask is None:
            voxel_mask = np.ones(examples.shape[1])

        n2, n_brain_dimensions = examples.shape
        n1, n_dsm_dimensions = targets.shape

        assert n1 == n2, "`targets` must have as many rows as `examples`"
        n = n1

        num_train = math.floor(len(examples) * 0.7)
        train_examples, train_targets = examples[:num_train], targets[:num_train]
        test_examples, test_targets = examples[num_train:], targets[num_train:]

        scores = np.zeros((n_dsm_dimensions, n_brain_dimensions))  # #targets x #voxel
        targets_z = scipy.stats.zscore(test_targets)

        for v in tqdm(range(n_brain_dimensions), desc="Voxels"):

            if not voxel_mask[v]:
                continue

            model = sklearn.linear_model.Ridge()
            model.fit(train_examples[:, v].reshape(-1, 1), train_targets)
            predicted = model.predict(test_examples[:, v].reshape(-1, 1))
            scores[:, v] = np.sum(targets_z * scipy.stats.zscore(predicted), 0) / (n - 1)

        self.scores = scores


class VariabilityVoxelScorer(VoxelScorer):
    
    def __init__(self, threshold):
        
        self.threshold = threshold

    def calculate_scores(self, examples, targets, meta=None, lmbda=1, voxel_mask=None):

        examples_masked = examples[:, voxel_mask.nonzero()[0]]
        variability = np.expand_dims(np.var(examples_masked, axis=0), 0)
        
        if self.threshold:
            variability[variability > self.threshold] = 0

        self.scores = variability
