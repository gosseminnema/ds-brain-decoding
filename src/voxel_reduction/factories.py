import abc

from voxel_reduction import voxel_scoring
from voxel_reduction import voxel_selection


class ScorerFactory(abc.ABC):
    """
    Object for creating `VoxelScorer` instances. Using the Factory design pattern allows the ExperimentRunner.run()
    method to be generic (i.e. agnostic as to which subclass of `VoxelScorer` should be used).
    This abstract base class specifies the interface that deriving classes should implement.
    """

    @abc.abstractmethod
    def create_scorer(self):
        """
        :rtype: voxel_scoring.VoxelScorer
        """
        pass


class Pereira18ScorerFactory(ScorerFactory):
    """
    Generates new instances of the Pereira18VoxelScorer class
    """

    def create_scorer(self):
        return voxel_scoring.Pereira18VoxelScorer()


class SimplifiedScorerFactory(ScorerFactory):
    """
    Generates new instances of the SimplifiedScorerFactory class
    """
    
    def create_scorer(self):
        return voxel_scoring.SimplifiedVoxelScorer()


class VariabilityScorerFactory(ScorerFactory):

    def __init__(self, threshold):
        self.threshold = threshold
    
    def create_scorer(self):
        return voxel_scoring.VariabilityVoxelScorer(self.threshold)


class SelectorFactory(abc.ABC):
    """
    Object for creating `VoxelSelector` instances. Using the Factory design pattern allows the ExperimentRunner.run()
    method to be generic (i.e. agnostic as to which subclass of `VoxelSelector` should be used).
    This abstract base class specifies the interface that deriving classes should implement.
    """

    @abc.abstractmethod
    def create_selector(self):
        """
        :rtype voxel_selection.VoxelSelector
        """
        pass


class Pereira18SelectorFactory(SelectorFactory):
    """
    Generates new instances of the Pereira18VoxelSelector class
    """

    def create_selector(self):
        return voxel_selection.Pereira18VoxelSelector()

