import abc
import math

import numpy as np
import scipy.stats
import sklearn

from tqdm import tqdm

import pdb


class VoxelSelector(abc.ABC):
    """
    Given a matrix of scores for each (voxel * dimension) pair, find the best voxels using a particular
    algorithm.
    """

    @abc.abstractmethod
    def select_best_voxels(self, score_matrix):
        """
        Given a score matrix, find the indices of the best voxels
        :param score_matrix: matrix of shape (#dsm_dimensions, #voxels)
        :return: array of voxel indices sorted by 'goodness', in increasing order
        """
        pass


class Pereira18VoxelSelector(VoxelSelector):
    """
    Implements the voxel selection algorithm from Pereira et al.'s paper. Selects voxels that are strong predictors of
    any dimension.
    """

    def select_best_voxels(self, score_matrix):
        return score_matrix.max(axis=0).argsort()


