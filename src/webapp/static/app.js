function displayNearestNeighbours(wordNeighbours) {
    let targetNeighbours = wordNeighbours["target"];
    let predictionNeighbours = wordNeighbours["prediction"];
    targetNeighbours.reverse();
    predictionNeighbours.reverse();

    let $targetNeighbours = $("#target-neighbours");
    let $predictionNeighbours = $("#prediction-neighbours");

    $targetNeighbours.empty();
    targetNeighbours.forEach(x => {
       $targetNeighbours.append($("<li>").html(`<b>${x[1]}</b> (${x[2]})`));
    });

    $predictionNeighbours.empty();
    predictionNeighbours.forEach(x => {
       $predictionNeighbours.append($("<li>").html(`<b>${x[1]}</b> (${x[2]})`));
    });

}

function displayFigures(run, order, refreshVocab) {
    let cosineWord = $("#cosine-word").val();
    let rankWord = $("#rank-word").val();
    let neighboursWord = $("#neighbours-word").val();
    let neighboursParticipant = $("#neighbours-participant").val();

    $.get("analysis",
        // Data to send
        {
            run: run,
            order: order,
            cosineWord: cosineWord,
            rankWord: rankWord,
            neighboursWord: neighboursWord,
            neighboursParticipant: neighboursParticipant
        },

        // Handler
        function (data) {
            if (cosineWord == null || refreshVocab) {
                addWords(data["vocabulary"], $("#cosine-word"));
            }
            if (neighboursWord == null || refreshVocab) {
                addWords(data["vocabulary"], $("#neighbours-word"))
            }
            if (neighboursParticipant == null || refreshVocab) {
                addParticipants(data["neighbour_scores"]["participants"], $("#neighbours-participant"))
            }
            if (rankWord == null || refreshVocab) {
                addWords(data["vocabulary"], $("#rank-word"));
            }

            displayScores("pairwise", data["pairwise_scores"]);
            displayMean("pairwiseMean", data["pairwise_scores"]);

            displayConfusionMatrix(data["confusion"]);

            displayScores("cosine", data["cosine_scores"]);
            displayMean("cosineMean", data["cosine_scores"]);

            displayScores("cosineByWord", data["cosine_scores_by_word"]);

            displayScores("rank", data["rank_scores"]);
            displayMean("rankMean", data["rank_scores"]);
            displayScores("rankByWord", data["rank_scores_by_word"]);

            displayScores("neighbours", data["neighbour_scores"]);
            displayMean("neighboursMean", data["neighbour_scores"]);

            displayNearestNeighbours(data["word_neighbours"]);

            displayScores("r2scores", data["r2_scores"], null);
            displayMean("r2mean", data["r2_scores"]);

            displayScores("patnscores", data["patn_scores"]);
            displayMean("patnmean", data["patn_scores"]);

            displayScores("rsascore", data["rsa_scores"]);
            displayMean("rsamean", data["rsa_scores"]);

            displayScores("rsapval", data["rsa_pvals"], null);
            displayMean("rsameanp", data["rsa_pvals"]);
        });
}

// Add words to the dropdown
function addWords(vocabulary, $select) {
    $select.empty();
    vocabulary.forEach(w => $select.append($("<option>", {value: w, text: w})))
}

// Add participants to the dropdown
function addParticipants(participants, $select) {
    $select.empty();
    participants.forEach(p => $select.append($("<option>", {value: p, text: p})))
}


function displayScores(div, scoreData, yRange=[0, 1]) {
    const data = [{
        x: scoreData["participants"],
        y: scoreData["scores"],
        type: "bar"
    }];
    const layout = {
        autosize: false,
        width: 500,
        height: 500,
        yaxis: {}
    };
    if (yRange !== null) {
        layout["yaxis"]["range"] = yRange;
    }

    Plotly.newPlot(div, data, layout);
}

function displayMean(div, scoreData) {
    $(`#${div}`).text(`Mean: ${scoreData["mean"]}`);
}

function displayConfusionMatrix(confusionData) {
    const data = [{
        x: confusionData["xLabels"],
        y: confusionData["yLabels"],
        z: confusionData["matrix"],
        type: "heatmap"
    }];
    const layout = {
        autosize: false,
        width: 500,
        height: 500
    };

    Plotly.newPlot("confusion", data, layout)
}

function onSelectChanged(refreshVocab) {
    return function() {
        const run = $("#runs").val();
        const order = $("#orders").val();
        displayFigures(run, order, refreshVocab);
    };
}

$(document).ready(function () {
    $("#runs").change(onSelectChanged(true));
    $("#cosine-word, #neighbours-word, #neighbours-participant, #rank-word, #orders").change(onSelectChanged(false));

    displayFigures("m08-p18-p18");
});


