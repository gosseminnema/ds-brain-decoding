import collections
import sys

import numpy as np
from flask import Flask, jsonify, request, render_template

# Needed to import modules from same directory
sys.path.append(".")
from run_analysis import run_analysis
from data_preparation import brain_dataset

# Define app
app = Flask(__name__)

# Define `Args` class
Args = collections.namedtuple(
    typename="Args",
    field_names=[
        "prev_run",
        "randomize",
        "dataset",
        "pereira18_paradigm"
    ])


# get the mean without counting the averaged predictions
def get_participant_mean(df, score_key):
    return df[df["participant"] != "averaged"][score_key].mean()

@app.route("/analysis")
def analyze():
    # Parse arguments from GET
    run = request.args.get("run")
    if run.startswith("m08v25"):
        dataset = "mitchell08v25"
        paradigm = None
    elif run.startswith("m08"):
        dataset = "mitchell08"
        paradigm = None
    else:
        dataset = "pereira18"
        paradigm = "averaged"

    cosine_word = request.args.get("cosineWord")
    rank_word = request.args.get("rankWord")
    neighbours_word = request.args.get("neighboursWord")
    n_subj = request.args.get("neighboursParticipant")

    order = request.args.get("order")

    # Compile args object
    args = Args(prev_run=run, randomize="true", dataset=dataset, pereira18_paradigm=paradigm)

    # Create a dataset object and get the vocabulary
    if dataset == "pereira18":
        ds = brain_dataset.Pereira18Dataset(paradigm)

    elif dataset == "mitchell08v25":
        ds = brain_dataset.Mitchell08Dataset(dsm_path="../data/Mitchell-etal_2008/Vectors/mitchell-25.txt",
                                             dsm_format="values_only", )

    else:
        ds = brain_dataset.Mitchell08Dataset()
    vocabulary = np.array(ds.vocabulary)

    # Run the analyzer
    p_analyzer, c_analyzer, n_analyzer, r_analyzer, r2_analyzer, pat1_analyzer, rsa_analyzer = run_analysis(args)

    # Pairwise
    p_scores_df = p_analyzer.create_scores_df()
    confusion = p_analyzer.create_confusion_mat(order=order)

    # Cosine
    c_scores_df = c_analyzer.create_scores_df()
    c_scores_by_word = c_analyzer.create_scores_by_words_df()
    if not cosine_word or cosine_word not in vocabulary:
        cosine_word = vocabulary[0]
    c_scores_word = c_scores_by_word[c_scores_by_word["word"] == cosine_word]["score"].item()

    # Nearest neighbours
    n_scores_df = n_analyzer.create_scores_df()
    neighbours_df = n_analyzer.create_neighbours_df()
    if not neighbours_word or neighbours_word not in vocabulary:
        neighbours_word = vocabulary[0]
    if not n_subj or n_subj not in n_scores_df["participant"].tolist():
        n_subj = n_scores_df["participant"].tolist()[0]
    n_word_idx = list(vocabulary).index(neighbours_word)
    target_neighbours = neighbours_df[neighbours_df["participant"] == "target"]["neighbours"].item()[n_word_idx]
    prediction_neighbours = neighbours_df[neighbours_df["participant"] == n_subj]["neighbours"].item()[n_word_idx]

    # Rank
    r_scores_df = r_analyzer.create_scores_df()
    r_scores_by_word = r_analyzer.create_scores_by_words_df()
    if not rank_word or rank_word not in vocabulary:
        cosine_word = vocabulary[0]
    r_scores_word = r_scores_by_word[c_scores_by_word["word"] == cosine_word]["score"].item()

    # R2 scores
    r2_scores_df = r2_analyzer.create_scores_df()

    # P@N
    patn_scores_df = pat1_analyzer.create_scores_df()

    # RSA
    rsa_scores_df = rsa_analyzer.create_scores_df()


    return jsonify({
        "vocabulary": vocabulary.tolist(),
        "pairwise_scores": {
            "participants": p_scores_df["participant"].tolist(),
            "scores": p_scores_df["score"].tolist(),
            "mean": get_participant_mean(p_scores_df, "score")
        },
        "confusion": {
            "matrix": confusion["matrix"].tolist(),
            "xLabels": vocabulary[confusion["id_cols"]].tolist(),
            "yLabels": vocabulary[confusion["id_rows"]].tolist()
        },
        "cosine_scores": {
            "participants": c_scores_df["participant"].tolist(),
            "scores": c_scores_df["score"].tolist(),
            "mean": get_participant_mean(c_scores_df, "score")
        },
        "cosine_scores_by_word": {
            "participants": c_scores_df["participant"].tolist(),
            "scores": c_scores_word
        },
        "neighbour_scores": {
            "participants": n_scores_df["participant"].tolist(),
            "scores": n_scores_df["score_avg"].tolist(),
            "mean": get_participant_mean(n_scores_df, "score_avg")
        },
        "word_neighbours": {
            "target": target_neighbours,
            "prediction": prediction_neighbours
        },
        "rank_scores": {
            "participants": r_scores_df["participant"].tolist(),
            "scores": r_scores_df["score"].tolist(),
            "mean": get_participant_mean(r_scores_df, "score")
        },
        "rank_scores_by_word": {
            "participants": r_scores_df["participant"].tolist(),
            "scores": r_scores_word
        },
        "r2_scores": {
            "participants": r2_scores_df["participant"].tolist(),
            "scores": r2_scores_df["score"].tolist(),
            "mean": get_participant_mean(r2_scores_df, "score")
        },
        "patn_scores": {
            "participants": patn_scores_df["participant"].tolist(),
            "scores": patn_scores_df["score"].tolist(),
            "mean": get_participant_mean(patn_scores_df, "score")
        },
        "rsa_scores": {
            "participants": rsa_scores_df["participant"].tolist(),
            "scores": rsa_scores_df["score"].tolist(),
            "mean": get_participant_mean(rsa_scores_df, "score")
        },
        "rsa_pvals": {
            "participants": rsa_scores_df["participant"].tolist(),
            "scores": rsa_scores_df["p_val"].tolist(),
            "mean": get_participant_mean(rsa_scores_df, "p_val")
        }
    })


@app.route("/display")
def display():
    return render_template("index.html")
