import argparse
import os
import shutil

import numpy as np

from analysis import pairwise, cosine, nearest_neighbours, rank, r2_scores, precision, rsa
from data_preparation import brain_dataset
from experiments import load_outputs


def main(args):
    p_analyzer, c_analyzer, n_analyzer, _, _, _ = run_analysis(args)

    p_scores_df = p_analyzer.create_scores_df()
    confusion_mat = p_analyzer.create_confusion_mat()

    c_scores_word = c_analyzer.create_scores_by_words_df()
    c_scores_avg = c_analyzer.create_scores_df()

    neighbours = n_analyzer.create_neighbours_df()
    n_scores_df = n_analyzer.create_scores_df()

    # Save outputs
    output_dir = f"../analysis/{args.run_name}"
    if os.path.exists(output_dir):
        # Overwrite directory of it already exists
        shutil.rmtree(output_dir)
    os.makedirs(output_dir)
    p_scores_df.to_json(os.path.join(output_dir, "p_scores_df.json"))
    c_scores_word.to_json(os.path.join(output_dir, "c_scores_word_df.json"))
    c_scores_avg.to_json(os.path.join(output_dir, "c_scores_avg_df.json"))
    neighbours.to_json(os.path.join(output_dir, "neighbours_df.json"))
    n_scores_df.to_json(os.path.join(output_dir, "n_scores_df.json"))
    np.save(os.path.join(output_dir, "confusion_mat.npy"), confusion_mat)


def run_analysis(args):
    """
    Run the analysis
    :param args: object representing the arguments (from argparse or using a named tuple)
    :return: PairwiseAnalyzer object
    """

    dataset, participants, predictions, targets = load_data(args)

    # Do the analysis
    p_analyzer = pairwise.PairwiseAnalyzer()
    c_analyzer = cosine.CosineAnalyzer(dataset.vocabulary)
    n_analyzer = nearest_neighbours.NearestNeighboursAnalyzer(targets, dataset.vocabulary)
    r_analyzer = rank.RankAnalyzer(dataset.vocabulary)
    r2_analyzer = r2_scores.R2Analyzer()
    pat1_analyzer = precision.PrecisionAnalyzer(n=10)
    rsa_analyzer = rsa.RSAAnalyzer()
    for p in participants:
        p_analyzer.analyze_participant(p, predictions[p], targets, paradigm=args.pereira18_paradigm)
        c_analyzer.analyze_participant(p, predictions[p], targets, paradigm=args.pereira18_paradigm)
        n_analyzer.analyze_participant(p, predictions[p])
        r_analyzer.analyze_participant(p, predictions[p], targets, paradigm=args.pereira18_paradigm)
        r2_analyzer.analyze_participant(p, predictions[p], targets, paradigm=args.pereira18_paradigm)
        pat1_analyzer.analyze_participant(p, predictions[p], targets)
        rsa_analyzer.analyze_participant(p, predictions[p], targets)
    return p_analyzer, c_analyzer, n_analyzer, r_analyzer, r2_analyzer, pat1_analyzer, rsa_analyzer


def load_data(args):
    loader = load_outputs.OutputLoader(os.path.join("../output", args.prev_run))

    # Randomize or not?
    if args.randomize.lower() == "true":
        randomize = True
    elif args.randomize.lower() == "false":
        randomize = False
    else:
        raise ValueError("Value of `randomize` should be either 'false' or 'true'")

    # Load participants and random indices
    participants = loader.load_participants()
    if randomize:
        random_ix = loader.load_random_idx()
    else:
        random_ix = None

    # Load predictions
    predictions = {}
    for p in participants:
        print(p)
        predictions[p] = loader.load_predictions(p, random_ix)

    # Add average predictions across participants
    predictions["averaged"] = np.mean(np.array(list(predictions.values())), axis=0)
    participants.append("averaged")
    print(predictions["averaged"].shape)

    # Load targets
    if args.dataset == "pereira18":
        dataset = brain_dataset.Pereira18Dataset(args.pereira18_paradigm)
    elif args.dataset == "mitchell08v25":
        dataset = brain_dataset.Mitchell08Dataset(dsm_path="../data/Mitchell-etal_2008/Vectors/mitchell-25.txt",
                                                  dsm_format="values_only")
    elif args.dataset == "mitchell08":
        dataset = brain_dataset.Mitchell08Dataset()
    else:
        raise ValueError("`dataset` must be one of ['pereira18', 'mitchell08', 'mitchell08v25']")
    print(dataset.dsm_data.shape)

    targets = dataset.dsm_data.squeeze()
    return dataset, participants, predictions, targets


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("run_name",
                        help="Name of the current run, will be used for naming output files (N.B.: using a run name "
                             "that already exists will cause the corresponding files to be overwritten")

    parser.add_argument("dataset",
                        help="Dataset to use, should be one of ['pereira18', 'mitchell08']")

    parser.add_argument("prev_run",
                        help="If you want to use saved data from a previous run, specify the name of that run",
                        default="")

    parser.add_argument("--pereira18_paradigm",
                        help="If the pereira18 dataset is selected, which stimuli should be used? One of ['averaged', "
                             "'pictures', 'sentences', 'wordclouds']",
                        default="")

    parser.add_argument("--randomize",
                        help="Should the dataset be randomized? One of ['true', 'false']",
                        default="true")

    main(parser.parse_args())
