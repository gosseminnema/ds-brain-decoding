import collections
import os
import shutil
import argparse
import json

import numpy as np

from tqdm import tqdm

from experiments import experiment
from models import factories as m_factories
from voxel_reduction import factories as rx_factories
from data_preparation import brain_dataset
from run_analysis import run_analysis


AnalysisArgs = collections.namedtuple(
    typename="Args",
    field_names=[
        "prev_run",
        "randomize",
        "dataset",
        "pereira18_paradigm"
    ])


def main(args):
    
    participants = ["M01"]
    
    setups = []
    with open(f"../tuning_setups/{args.config_name}.json") as f:
        setups = json.load(f)
    
    for i, setup in enumerate(tqdm(setups, desc="Setups: ")):
                
        # Define runner params
        dataset = brain_dataset.Pereira18Dataset("averaged")
        run_name = f"tuning_{args.config_name}_{i:02}"
        randomize = True
        center = True
        prev_run = None
        load_best_voxels = None
        n_folds = 18
        
        # Factories
        m_factory = m_factories.PerceptronFactory(
            use_dropout=setup["use_dropout"], 
            non_linear=setup["non_linear"], 
            hidden=setup["hidden_layers"],
            epochs=setup["epochs"]
        )        
        
        # Run the model
        runner = experiment.ExperimentRunner(dataset, run_name, randomize, center, load_best_voxels, n_folds)
        runner.run(participants, m_factory, None, None)
        
        # Analyze the outputs
        analysis_args = AnalysisArgs(prev_run=run_name, randomize="true", dataset="pereira18", pereira18_paradigm="averaged")
        p_analyzer = run_analysis(analysis_args)
        scores_df = p_analyzer.create_scores_df()
        confusion_mat = p_analyzer.create_confusion_mat()

        # Save the analysis
        output_dir = f"../analysis/{run_name}"
        if os.path.exists(output_dir):
            # Overwrite directory of it already exists
            shutil.rmtree(output_dir)
        os.makedirs(output_dir)
        scores_df.to_json(os.path.join(output_dir, "scores_df.json"))
        np.save(os.path.join(output_dir, "confusion_mat.npy"), confusion_mat)

        

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("config_name")
    args = parser.parse_args()
    main(args)