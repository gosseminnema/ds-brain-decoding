import abc
import itertools
import json

import numpy as np
import scipy.io


class BrainDataset(abc.ABC):
    """
    Represents a dataset and provides methods for loading and pre-processing participant data
    folds
    """

    def __init__(self, dsm_path, dsm_format, vocab_path, load_extra_data):

        # Distributional vectors, array of shape (num_words, num_dimensions)
        self._dsm_data = self._load_dsm_data(dsm_path, dsm_format)

        # List of words in the vocabulary
        self._vocabulary = self._load_vocabulary(vocab_path)

        # Brain images of the current participant, array of shape (num_words, num_dimensions)
        self._brain_data = []

        # only used by Pereira et al. dataset (for now)
        self.meta = None
        self.voxel_mask = None
        self.load_extra_data = load_extra_data
        self.extra_brain_data = None
        self.extra_dsm_data = None

        # Is the set randomized or not?
        self.randomized = False
        self.random_indices = None

        self.include_neighbours = 0

    def randomize(self, prev_idx=None):
        """
        Randomize the dataset. The original versions of `_dsm_data`, `_vocabulary` and `_brain_data` are unaffected;
        the randomized versions can be accessed using the corresponding properties.
        :param prev_idx: optional, list of random indices defined in a previous run
        :return:
        """

        self.randomized = True

        # Use existing indices if provided
        if prev_idx is not None:
            self.random_indices = prev_idx

        # Generate new random indices
        else:
            idx = np.arange(0, len(self._dsm_data))
            np.random.shuffle(idx)
            self.random_indices = idx

    @property
    def dsm_data(self):
        if self.randomized:
            return self._dsm_data[self.random_indices]
        return self._dsm_data

    @property
    def brain_data(self):
        if self.randomized:
            return [paradigm[self.random_indices] for paradigm in self._brain_data]
        return self._brain_data

    @property
    def vocabulary(self):
        if self.randomized:
            return self._vocabulary[self.random_indices]
        return self._vocabulary

    @abc.abstractmethod
    def load_mixed_participants(self, participants=None):
        """
        Loads the data files for all participants at the same time
        :return:
        """
        pass

    @abc.abstractmethod
    def load_participant_data(self, participant_id):
        """
        Loads the data file for a particular participant into memory
        :param participant_id: the participant's code (e.g. 'P1', 'M17')
        """
        pass

    @staticmethod
    def _load_dsm_data(dsm_path, dsm_format):
        """
        Load distributional vectors
        :param dsm_path: path to the model, should be a space-separated text file
        :param dsm_format: specifies the format; "words+values" = each line consists of a word followed by its vector;
        "values_only" = only the vectors themselves
        :return: array of shape (num_words, num_dimensions)
        """

        if dsm_format == "values_only":
            return np.expand_dims(np.loadtxt(dsm_path), axis=1)

        if dsm_format == "words+values":
            targets = []

            with open(dsm_path, encoding="utf-8") as f:
                for i, line in enumerate(f):
                    targets.append([np.array([float(v) for v in line.split()[1:]])])
            return np.array(targets)

        raise ValueError("`dsm_format` must be one of ['values_only', 'words+values'")

    @staticmethod
    def _load_vocabulary(vocab_path):
        with open(vocab_path, encoding="utf-8") as f:
            words = [w.strip() for w in f.readlines()]
            return words

    @abc.abstractmethod
    def add_nearest_neighbours(self, n=3):
        pass

    def _add_nearest_neighbours(self, n, folder_name):

        vocabulary = []
        vectors = []

        with open(f"../data/{folder_name}/nearest_neighbours.json", encoding="utf-8") as f:
            word_to_neighbours = json.load(f)

        with open(f"../data/{folder_name}/stimuli_180+neighbours_w2i.json", encoding="utf-8") as f:
            word_to_index = json.load(f)

        neighbour_vectors = np.load(f"../data/{folder_name}/vectors_180+neighbours.npy")

        for i, word in enumerate(self._vocabulary):
            vocabulary.append(word)
            current_word_vectors = [np.squeeze(self._dsm_data[i])]

            neighbours = word_to_neighbours[word][:n]
            for (w, _) in neighbours:
                vocabulary.append(w)
                current_word_vectors.append(neighbour_vectors[word_to_index[w]])

            vectors.append(current_word_vectors)

        self._vocabulary = vocabulary
        self._dsm_data = np.array(vectors)
        self.include_neighbours = n


class Pereira18Dataset(BrainDataset):
    """
    Represents and provides methods for manipulating the Pereira et al. (2018) dataset
    """

    def __init__(self, paradigm, apply_cortical_masking=True, load_extra_data=False,
                 dsm_path="../data/Pereira-etal_2018/vectors_180concepts.GV42B300.txt",
                 dsm_format="values_only", vocab_path="../data/Pereira-etal_2018/stimuli_180concepts.txt"):
        """
        :param paradigm: stimulus presentation paradigm, one of ["averaged", "all", "pictures", "sentences",
        "wordclouds"]
        :param apply_cortical_masking: if yes, a cortical mask is automatically applied after loading the data
        :param dsm_path: file path to the word vector file to be used
        :param dsm_format: format of the word vector file
        :param vocab_path: file path to the file listing the words in the vocabulary
        """

        super().__init__(dsm_path, dsm_format, vocab_path, load_extra_data)

        # Stimulus presentation paradigm (one of ["averaged", "pictures", "sentences", "wordclouds"])
        self.paradigm = paradigm

        # Load extra data (from experiment 2)
        if self.load_extra_data == 2:
            self.extra_dsm_data = self._load_dsm_data("../data/Pereira-etal_2018/vectors_384sentences.txt", dsm_format)
        elif self.load_extra_data == 3:
            self.extra_dsm_data = self._load_dsm_data("../data/Pereira-etal_2018/vectors_234", dsm_format)
        elif self.load_extra_data:
            raise ValueError("`load_extra_data` should be in [0, 2, 3]")

        # Brain meta data of the current participant
        self.meta = None

        # Cortical masking
        self.apply_cortical_masking = apply_cortical_masking
        self.voxel_mask = None

        # Voxel selection
        self.voxel_scores = None
        self.best_voxel_idx = None

    def load_mixed_participants(self, participants=None):

        if not participants:
            participants = ["M01", "M02", "M03", "M04", "M05", "M06", "M07", "M08", "M09", "M10", "M13", "M14", "M15", "M16", "M17", "P01"]

        # Disable cortical masking
        self.apply_cortical_masking = False
            
        # Load brain data and pad so that everything has the same width
        brain_data = [example_list for p in participants for example_list in self._load_participant(p)[0]]

        max_width = 212742
        for i, example_list in enumerate(brain_data):
            _, width = example_list.shape
            pad_size = max_width - width
            brain_data[i] = np.pad(example_list, ((0, 0), (0, pad_size)), "constant", constant_values=0)

        self._brain_data = brain_data

    def load_participant_data(self, participant_id):

        brain_data, meta = self._load_participant(participant_id)

        self.meta = meta

        if self.include_neighbours:
            self._brain_data = brain_data * (self.include_neighbours + 1)
        else:
            self._brain_data = brain_data

        if self.load_extra_data:
            self.extra_brain_data = \
                [np.load(f"../data/Pereira-etal_2018/{participant_id}/examples_384sentences.mat", mmap_mode="r")]

    def _load_participant(self, participant_id):

        if self.paradigm == "averaged":
            brain_data = [np.load(f"../data/Pereira-etal_2018/{participant_id}/examples_180concepts_averaged.npy")]

            # Use 'meta' from the 'pictures' paradigm
            pictures_path = f"../data/Pereira-etal_2018/{participant_id}/examples_180concepts_pictures.mat"
            meta = scipy.io.loadmat(pictures_path)["meta"]

        # Mix images from different paradigms
        elif self.paradigm == "all":
            data_paths = [f"../data/Pereira-etal_2018/{participant_id}/examples_180concepts_{prd}.npy"
                          for prd in ["pictures", "wordclouds", "sentences"]]
            brain_data = [np.load(path, mmap_mode="r") for path in data_paths] + \
                         [np.load(f"../data/Pereira-etal_2018/{participant_id}/examples_180concepts_averaged.npy")]

            # Use 'meta' from the 'pictures' paradigm
            pictures_path = f"../data/Pereira-etal_2018/{participant_id}/examples_180concepts_pictures.mat"
            meta = scipy.io.loadmat(pictures_path)["meta"]

        else:
            data_path = f"../data/Pereira-etal_2018/{participant_id}/examples_180concepts_{self.paradigm}.npy"
            data = np.load(data_path, mmap_mode="r")
            brain_data = [data["examples"]]
            meta = data["meta"]

        # Apply a cortical mask to remove irrelevant voxels and reduce the number of dimensions to ~50,000
        if self.apply_cortical_masking:
            voxel_mask = np.zeros(brain_data[0].shape[1])
            indices_in_gordon = meta["roiMultimaskGordon"][0][0].ravel().nonzero()[0]
            voxel_mask[indices_in_gordon] = 1
            self.voxel_mask = voxel_mask
        return brain_data, meta

    def add_nearest_neighbours(self, n=3):
        self._add_nearest_neighbours(n, "Pereira-etal_2018")


class Mitchell08Dataset(BrainDataset):

    def __init__(self, dsm_path="../data/Mitchell-etal_2008/Vectors/glove-300.txt",
                 dsm_format="words+values", vocab_path="../data/Mitchell-etal_2008/dictionary.txt"):
        super().__init__(dsm_path, dsm_format, vocab_path, False)

        self.categories = None

    def load_mixed_participants(self, participants=None):

        if not participants:
            participants = ["P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9"]
        brain_data = [example_list for p in participants for example_list in self._load_participant(p)]
        
        max_width = 21764
        for i, example_list in enumerate(brain_data):
            _, width = example_list.shape
            pad_size = max_width - width
            brain_data[i] = np.pad(example_list, ((0, 0), (0, pad_size)), "constant", constant_values=0)
        
        self._brain_data = brain_data

    def load_participant_data(self, participant_id):
        brain_data = self._load_participant(participant_id)

        if self.include_neighbours:
            self._brain_data = brain_data * (self.include_neighbours + 1)
        else:
            self._brain_data = brain_data

    def _load_participant(self, participant_id):
        data = scipy.io.loadmat(f"../data/Mitchell-etal_2008/data-science-{participant_id}.mat")
        triples = self._make_triples(data)
        brain_data = [self._average_repetitions(triples)]
        return brain_data

    @staticmethod
    def _make_triples(data):
        """
        Match brain images and their corresponding word and category labels
        :param data: full data file as loaded from MATLAB
        :return: list of (brain_image, word, category) triples
        """

        brain_data = np.array([arr[0][0] for arr in data["data"]])
        words = [arr[2][0] for arr in data["info"][0]]
        categories = [arr[0][0] for arr in data["info"][0]]

        # Make (data, label, category) triples
        triples = zip(brain_data, words, categories)
        triples = sorted(triples, key=lambda triple: triple[1])
        return triples

    @staticmethod
    def _average_repetitions(triples):
        """
        Average brain images across repetitions
        :param triples: list of (brain_image, word, category) triples
        :return: list of averaged brain images
        """

        # Group triples by stimulus
        grouped = []
        for _, group in itertools.groupby(triples, key=lambda triple: triple[1]):
            grouped.append((list(group)))

        # Average fMRI images in each group
        averaged = np.zeros((len(grouped), len(triples[0][0])))

        # Labels and categories corresponding to the averaged images
        averaged_labels = []
        averaged_categories = []

        for i, group in enumerate(grouped):
            data = np.array([triple[0] for triple in group])
            averaged[i] = np.mean(data, axis=0)
            averaged_labels.append(group[0][1])
            averaged_categories.append(group[0][2])
        return averaged

    def add_nearest_neighbours(self, n=3):
        self._add_nearest_neighbours(n, "Mitchell-etal_2008")
