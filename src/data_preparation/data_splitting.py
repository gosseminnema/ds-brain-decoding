import pdb

import numpy as np
import collections

from data_preparation import brain_dataset


class DataSplitter:
    """
    Splits the dataset into 'train' and 'test' sets for each cross-validation fold. This class is an iterator; for
    each fold it will provide a SplitData object providing access to the data in that split of the dataset
    """

    dataset: brain_dataset.BrainDataset

    def __init__(self, dataset, n_folds=1):
        """
        :param dataset: BrainDataset object representing the dataset to be split
        :param n_folds: number of folds to split the set in
        """
        self.dataset = dataset
        self.n_folds = n_folds

        # Make groups based on the data set size and the number of folds
        n = len(self.dataset.brain_data[0])  # number of paradigms * number of images
        self._labels_group = 1 + np.arange(1, n + 1) % self.n_folds
        self._groups = collections.deque(np.unique(self._labels_group))

    def __iter__(self):
        return self

    def __next__(self):

        # Get the next group
        try:
            group = self._groups.popleft()
        except IndexError:
            raise StopIteration

        # Mask for selecting the right indices
        mask = self._labels_group == group
        idx_test = np.where(mask)[0]
        idx_train = np.where(np.invert(mask))[0]

        # Split in train and test
        train_examples = np.array(list(zip(*self.dataset.brain_data)))[idx_train]
        test_examples = self.dataset.brain_data[-1][idx_test]
        train_targets = self.dataset.dsm_data[idx_train]
        test_targets = self.dataset.dsm_data[idx_test]

        # Keep track of the original indices (i.e. before randomization)
        if self.dataset.randomized:
            orig_idx_train = np.tile(self.dataset.random_indices[idx_train], len(self.dataset.brain_data))
            orig_idx_test = np.tile(self.dataset.random_indices[idx_test], len(self.dataset.brain_data))
        else:
            orig_idx_train = idx_train
            orig_idx_test = idx_test

        # Extra data
        extra_examples = self.dataset.extra_brain_data
        extra_targets = self.dataset.extra_dsm_data

        return SplitData(idx_train, idx_test, orig_idx_train, orig_idx_test, train_examples, test_examples,
                         extra_examples, train_targets, test_targets, extra_targets)


class SplitData:
    """
    Represents and provides methods for manipulating the dataset in a particular cross-validation fold
    """

    def __init__(self, idx_train, idx_test, orig_idx_train, orig_idx_test, brain_train, brain_test, brain_extra,
                 dsm_train, dsm_test, dsm_extra):
        """
        :param idx_train: indices of the words in the training set
        :param idx_test: indices of the words in the test set
        :param orig_idx_train: indices in the un-randomized training set (== idx_train if no randomization was applied)
        :param orig_idx_test: indices in the un-randomized test set (== idx_test if no randomization was applied)
        :param brain_train: brain images in the training set
        :param brain_test: brain images in the testing set
        :param dsm_train: meaning vectors in the training set
        :param dsm_test: meaning vectors in the testing set
        """

        self._data = {
            "idx": {
                "train": idx_train,
                "test": idx_test
            },
            "idx_orig": {
                "train": orig_idx_train,
                "test": orig_idx_test
            },
            "brain": {
                "train": brain_train,
                "test": brain_test,
                "extra": brain_extra
            },
            "dsm": {
                "train": dsm_train,
                "test": dsm_test,
                "extra": dsm_extra
            },
        }
        self.centered = False

    @property
    def data(self):
        """
        Dictionary representing that dataset in the current cross-validation fold. Contains the entries ["idx",
        "idx_orig", "brain", "dsm"]. Each of these is its own dictionary with entries ["train", "test"]. See
        documentation for the __init__() method of this class for more information.
        :return: dictionary
        """
        return self._data

    def center(self, only_brain=False):
        """
        Centers the dataset by calculating a mean brain image and meaning vector and then subtracting these from their
        respective training and testing sets.
        :param only_brain: if set to True, only the brain images, but not the meaning vectors, will be centered
        :return:
        """
        
        image_mean = np.mean(self._data["brain"]["train"], 0)

        if only_brain:
            # Zero array of appropriate length
            vector_mean = np.zeros(len(self._data["dsm"]["train"][0]))
        else:
            vector_mean = np.mean(self._data["dsm"]["train"], 0)

        self._data["brain"] = {
            "train": self._data["brain"]["train"] - image_mean,
            "test": self._data["brain"]["test"] - image_mean[-1],
            "extra": self._data["brain"]["extra"] - image_mean[-1] if self._data["brain"]["extra"] is not None else None
        }

        self._data["dsm"] = {
            "train": self._data["dsm"]["train"] - vector_mean,
            "test": self._data["dsm"]["test"] - vector_mean,
            "extra": self._data["dsm"]["extra"] - vector_mean[-1] if self._data["dsm"]["extra"] is not None else None
        }

        self.centered = True
