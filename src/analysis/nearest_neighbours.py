import numpy as np
import pandas as pd
from sklearn.metrics import pairwise
from scipy.spatial import distance


class NearestNeighboursAnalyzer:

    N = 10

    def __init__(self, targets, vocabulary):

        self.targets = targets
        self.vocabulary = vocabulary

        # Baseline for target space
        self.target_neighbours = {
            "participant": "target",
            "neighbours": self._find_nearest_neighbours(targets)
        }

        self.prediction_neighbours = []
        self.jaccard_scores = []
        self.participants = []

    def analyze_participant(self, participant_id, predictions):

        # Find neighbours for predictions
        neighbours = self._find_nearest_neighbours(predictions)
        self.prediction_neighbours.append({
            "participant": participant_id,
            "neighbours": neighbours
        })

        # Compute distances between neighbour sets
        similarities = self._compare_neighbours(neighbours, self.target_neighbours["neighbours"])
        self.jaccard_scores.append({
            "participant": participant_id,
            "scores": similarities,
            "score_avg": similarities.mean()
        })

    def create_neighbours_df(self):
        return pd.DataFrame(data=[self.target_neighbours] + self.prediction_neighbours)

    def create_scores_df(self):
        return pd.DataFrame(data=self.jaccard_scores)

    def _find_nearest_neighbours(self, vectors):

        neighbours = []
        cosines = pairwise.cosine_similarity(vectors)

        for row in cosines:
            closest = np.argsort(row)[-(self.N + 1):-1]
            neighbours.append([(int(i), self.vocabulary[i], float(row[i])) for i in closest])

        return neighbours

    def _compare_neighbours(self, x_neighbours, y_neighbours):

        similarities = np.zeros(len(self.vocabulary))

        for i, word in enumerate(self.vocabulary):

            # Represent neighbours as boolean arrays
            x_bool = np.zeros(len(self.vocabulary))
            for idx, _, _ in x_neighbours[i]:
                x_bool[idx] = 1

            y_bool = np.zeros(len(self.vocabulary))
            for idx, _, _ in y_neighbours[i]:
                y_bool[idx] = 1

            d = distance.jaccard(x_bool, y_bool)
            similarities[i] = 1 - d

        return similarities



