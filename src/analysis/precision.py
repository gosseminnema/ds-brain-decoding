import numpy as np
import pandas as pd


class PrecisionAnalyzer:

    def __init__(self, n):
        self.n = n
        self.scores = []

    def analyze_participant(self, participant_id, predictions, targets):

        self.scores.append({
            "participant": participant_id,
            "score": self._analyze_precision(predictions, targets)
        })

    def create_scores_df(self):
        return pd.DataFrame(data=self.scores)

    def _analyze_precision(self, predictions, targets):

        # TODO: copied from `rank.py`, rewrite (DRY!)
        correlations = np.corrcoef(predictions, targets)
        # slice the matrix: we only want row-by-row correlations between the two matrices, not within each matrix
        correlations = correlations[:len(predictions), len(targets):]

        # sort every row (=prediction) and save the stimulus indices (reverse order so that we get indices from best
        # to worst)
        best_correlation_ix = np.fliplr(np.argsort(correlations, axis=1))

        # find the ranks of the correct stimuli
        ix_column = np.arange(len(predictions)).reshape(-1, 1)  # prediction indices as a column
        correct_ranks = np.argmax(ix_column == best_correlation_ix, axis=1)  # get the rank

        precision_score = np.count_nonzero(correct_ranks < self.n) / len(predictions)
        return precision_score

