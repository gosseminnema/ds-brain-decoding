import numpy as np
import pandas as pd


class PairwiseAnalyzer:

    def __init__(self):
        self.participants = []
        self.scores = []
        self.confusion_mats = []

    def analyze_participant(self, participant_id, predictions, targets, paradigm=None, category=None):
        """
        Add a participant and analyze their pairwise performance
        :param participant_id: participant ID
        :param predictions: predicted vectors array (n_examples, n_dimensions)
        :param targets: target vectors array (n_examples, n_dimensions)
        :param paradigm: stimulus paradigm (only applies to Pereira et al. 2018 dataset)
        :param category: stimulus category
        :return:
        """

        score, mistakes = self._evaluate_pairwise(predictions, targets)

        self.scores.append({
            "participant": participant_id,
            "paradigm": paradigm,
            "category": category,
            "score": score,
        })

        self.participants.append(participant_id)
        self.confusion_mats.append(self._get_mistake_mat(len(targets), mistakes))

    def create_scores_df(self):
        """
        Create a Pandas data frame containing the pairwise scores of all analyzed participants
        :return: df with columns "participant", "paradigm", "category", "score"
        :rtype: pd.DataFrame
        """
        return pd.DataFrame(data=self.scores)

    def create_confusion_mat(self, order="default"):
        """
        Create a confusion matrix for all participants analyzed so far, in which each cell (i, j) represents the number
        of participants where the model confused prediction i and target j
        :return: array with shape (180, 180)
        """
        matrix = np.array(self.confusion_mats).sum(axis=0)
        id_rows = id_cols = np.arange(len(matrix))

        if order == "mistakes":
            id_cols = np.argsort(np.mean(matrix, axis=0))
            id_rows = np.argsort(np.mean(matrix, axis=1))

        return {
            "matrix": matrix[id_rows][:, id_cols],
            "id_rows": id_rows,
            "id_cols": id_cols
        }

    @staticmethod
    def _evaluate_pairwise(predictions, targets):
        """

        :param predictions:
        :param targets:
        :return:
        """

        correlations = np.corrcoef(predictions, targets)

        # slice the matrix: we only want row-by-row correlations between the two matrices, not within each matrix
        correlations = correlations[:len(predictions), len(targets):]

        comparisons = 0
        score = 0
        mistakes = []

        for i in range(len(correlations)):
            for j in range(len(correlations)):

                if i == j:
                    continue

                # TODO: check what is going on!
                if max([correlations[i, i], correlations[i, j], correlations[j, i], correlations[j, j]]) \
                        == max([correlations[i, i]], correlations[j, j]):
                    score += 1

                else:
                    mistakes.append((i, j))

                comparisons += 1

        score /= comparisons
        return score, mistakes

    @staticmethod
    def _get_mistake_mat(n, mistakes):
        """

        :param n:
        :param mistakes:
        :return:
        """

        matrix = np.zeros((n, n))
        for (i, j) in mistakes:
            matrix[i, j] += 1

        # Normalize and return
        return matrix
