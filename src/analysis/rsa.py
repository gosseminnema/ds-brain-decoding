import numpy as np
import scipy.stats
import pandas as pd


class RSAAnalyzer:

    def __init__(self):
        self.scores = []

    def analyze_participant(self, participant_id, predictions, targets):

        score, p = self._analyze_rsa(predictions, targets)

        self.scores.append({
            "participant": participant_id,
            "score": score,
            "p_val": p
        })

    def create_scores_df(self):
        return pd.DataFrame(data=self.scores)

    @staticmethod
    def _analyze_rsa(predictions, targets):

        # Compute pairwise correlations
        corr_pred = np.corrcoef(predictions)
        corr_targ = np.corrcoef(targets)

        # Flatten upper triangle of both correlation matrices
        upp_tri_pred = RSAAnalyzer._get_upper_triangle(corr_pred)
        upp_tri_targ = RSAAnalyzer._get_upper_triangle(corr_targ)

        rsa_value, p_val = scipy.stats.pearsonr(upp_tri_pred, upp_tri_targ)
        return rsa_value, p_val

    @staticmethod
    def _get_upper_triangle(matrix):
        triangle = []

        n, m = matrix.shape
        for i in range(n):
            for j in range(i):
                triangle.append(matrix[i, j])

        return np.array(triangle)
