import numpy as np
import pandas as pd


class RankAnalyzer:

    def __init__(self, vocabulary):

        self.participants = []
        self.scores_by_word = []
        self.scores_avg = []
        self.vocabulary = vocabulary

    @staticmethod
    def _evaluate_rank(predictions, targets):
        # TODO: refactor so that we need to calculate the correlation matrix only once
        correlations = np.corrcoef(predictions, targets)
        # slice the matrix: we only want row-by-row correlations between the two matrices, not within each matrix
        correlations = correlations[:len(predictions), len(targets):]

        # sort every row (=prediction) and save the stimulus indices (reverse order so that we get indices from best
        # to worst)
        best_correlation_ix = np.fliplr(np.argsort(correlations, axis=1))

        # find the ranks of the correct stimuli
        ix_column = np.arange(len(predictions)).reshape(-1, 1)  # prediction indices as a column
        correct_ranks = np.argmax(ix_column == best_correlation_ix, axis=1)  # get the rank
        rank_scores = 1 - (correct_ranks / (len(predictions) - 1))

        return rank_scores, best_correlation_ix

    # TODO: these methods were copied from cosine.py, maybe do something to reduce redundancy
    def create_scores_by_words_df(self):

        return pd.DataFrame(data=[{
            "word": self.vocabulary[w_i],
            "score": [self.scores_by_word[p_i][w_i] for p_i, _ in enumerate(self.participants)]
        } for w_i in range(len(self.vocabulary))])

    def create_scores_df(self):
        return pd.DataFrame(data=self.scores_avg)

    def analyze_participant(self, participant_id, predictions, targets, paradigm=None, category=None):

        scores, _ = self._evaluate_rank(predictions, targets)
        self.participants.append(participant_id)
        self.scores_by_word.append([s.item() for s in scores])
        self.scores_avg.append({
            "participant": participant_id,
            "paradigm": paradigm,
            "category": category,
            "score": np.mean(scores),
        })
