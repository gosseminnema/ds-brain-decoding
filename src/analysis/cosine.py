import numpy as np
import pandas as pd
from sklearn.metrics import pairwise


class CosineAnalyzer:

    def __init__(self, vocabulary):

        self.participants = []
        self.scores_by_word = []
        self.scores_avg = []
        self.vocabulary = vocabulary

    def analyze_participant(self, participant_id, predictions, targets, paradigm=None, category=None):

        scores = self._get_cosine_scores(predictions, targets)
        self.participants.append(participant_id)
        self.scores_by_word.append([s.item() for s in scores])
        self.scores_avg.append({
            "participant": participant_id,
            "paradigm": paradigm,
            "category": category,
            "score": np.mean(scores),
        })

    def create_scores_by_words_df(self):

        return pd.DataFrame(data=[{
            "word": self.vocabulary[w_i],
            "score": [self.scores_by_word[p_i][w_i] for p_i, _ in enumerate(self.participants)]
        } for w_i in range(len(self.vocabulary))])

    def create_scores_df(self):
        return pd.DataFrame(data=self.scores_avg)

    @staticmethod
    def _get_cosine_scores(predictions, targets):

        scores = []

        for prediction, target in zip(predictions, targets):
            scores.append(pairwise.cosine_similarity(prediction.reshape(1, -1), target.reshape(1, -1)))

        return scores

