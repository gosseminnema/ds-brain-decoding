import sklearn
import pandas as pd


class R2Analyzer:

    def __init__(self):
        self.scores = []

    def analyze_participant(self, participant_id, predictions, targets, paradigm=None, category=None):
        """
        ...
        :param participant_id:
        :param predictions:
        :param targets:
        :param paradigm:
        :param category:
        :return:
        """

        self.scores.append({
            "participant": participant_id,
            "paradigm": paradigm,
            "category": category,
            "score": sklearn.metrics.r2_score(targets, predictions)
        })

    def create_scores_df(self):
        """
        ...
        :return:
        """

        return pd.DataFrame(data=self.scores)
