import scipy.io
import numpy as np

from tqdm import tqdm

def main():

    participants = ["P01"] + [f"M{i:02}" for i in range(1, 18) if i not in [11, 12]]

    for participant_id in tqdm(participants):

        pictures = scipy.io.loadmat(f"{participant_id}/examples_180concepts_pictures.mat")["examples"]
        sentences = scipy.io.loadmat(f"{participant_id}/examples_180concepts_sentences.mat")["examples"]
        clouds = scipy.io.loadmat(f"{participant_id}/examples_180concepts_wordclouds.mat")["examples"]

        mean_image = np.mean(np.array([pictures, sentences, clouds]), axis=0)

        np.save(f"{participant_id}/examples_180concepts_averaged.npy", mean_image)


if __name__ == "__main__":
    main()