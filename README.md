Welcome to the repository for the paper "From brain space to distributional space:
the perilous journeys of fMRI decoding" (Gosse Minnema & Aurélie Herbelot 2019)
to be presented at the ACL Student Research Workshop 2019. 

Prerequisites:

- Python 3.6 
- SciPy (NumPy, Matplotlib, Pandas)
- scikit-learn
- PyTorch (version 1.0.1)
- tqdm (for fancy progress bars)
- flask (for webapp)

To run the code, do the following: 

1. Clone the repo
2. Get the fMRI data: 
    - for the Pereira et al. 2018 dataset (reported on in the paper): go to the 
    `data/Pereira-etal_2018` directory and run `sh download_fmri.sh` to download 
    all of the data files. 
    (N.B. the files are very large, so you might want to start with downloading
    just one. Each file is a tar ball that you need to unpack before you can
    use the files. For documentation see https://osf.io/crwz7)
    - for the Mitchell et al. dataset (alternative fMRI data and vocabulary): 
    go to the `data/Mitchell-etal_2008` folder and run `sh download_fmri.sh`
    (N.B. again, these are large files; every participant has a single .MAT
    file. For documentation see http://www.cs.cmu.edu/afs/cs/project/theo-73/www/science2008/data.html) 
3. Run the decoder:
    - go to the `src` directory and run `python run_decoder.py [output_dir] [dataset_name] [participant_list]`
    (for example `python run_decoder.py example_run pereira18 "M01 M02 M03"`)
4. [Optional] Download our output files (predictions for each of the decoder outputs we tested) from https://drive.google.com/file/d/14ehnKL70apJi6vOrlF1kSIhzBbq7sera/view?usp=sharing
5. For analyzing the results, use the web interface: 
    - go to the `src` directory
    - setup flask: `export FLASK_APP=webapp/app.py && flask run`
    - go to `http://127.0.0.1:5000/display` in your browser
    - N.B.: the webapp loads output files based on their folder names; by default, the app will look for the output folders that can be downloaded in step 4. 
      To change this behaviour and analyze your own output files, simply add new `<option value='run_name'>` entries in `src/webapp/templates/index.html`
    